<!-- You can start editing here. -->
<?php if (have_comments()) { ?>
    <ol class="commentlist">
        <?php wp_list_comments(); ?>
    </ol>
<?php } else { ?>
    <p><?php _e('Ainda não recebemos comentários. Seja o primeiro a deixar sua opinião.') ?></p>
<?php } ?>
<?php comment_form(); ?>