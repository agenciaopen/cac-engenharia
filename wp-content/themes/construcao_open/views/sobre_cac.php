<?php 
/*
* Template Name: Sobre
* Template para página de Sobre
*/
get_header();

    get_template_part('template_part/components/tab_info');
    //components page
    get_template_part('template_part/components/page_about_cac/main_banner');
    get_template_part('template_part/components/page_about_cac/section_about_imv_cac');
    get_template_part('template_part/components/page_about_cac/section_trust');
    get_template_part('template_part/components/page_about_cac/section_about');
    get_template_part('template_part/components/page_about_cac/section_advantages');
    //component out page
    ?>
    <section class="cta-container">
       <?php get_template_part('template_part/layout/section_cta'); ?>
    </section>
    <section class="full-container-no-bg">
        <h4>Empreendimentos</h4>
        <div class="swiper-container launch">

        <?php 

	$imv_posts = get_posts( array(
		'posts_per_page' => -1,
		'offset'         => 0,
        'post_type'		 => 'imoveis',
        'orderby'        => 'cat',
        'order' 	     => 'DSC'
	    ) 
    );
            
    if ( $imv_posts ) { ?>
    <section id="if_moby">
        <section class="resulting launch">
                <div class="swiper-wrapper content-loop">
                        <?php
                                foreach ( $imv_posts as $post ) : 
                                    setup_postdata( $post ); 
                                    $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
                                    $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
                                    $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
                                    $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
                                    $faixa_de_preco = wp_get_post_terms($post->ID, 'faixa_de_preco', array("fields" => "names"));
                                    if($quarto[0] == "01"){ $quarto[0] = "01 Quarto";}else if($quarto[0] == "02"){ $quarto[0] = "02 Quartos";} else if ($quarto[0] == "03"){ $quarto[0] = "03 Quartos";} else if ($quarto[0] == "04"){ $quarto[0] = "04 Quartos";} else if ($quarto[0] == "05"){ $quarto[0] = "05 Quartos";}else if ($quarto[0] == "06"){ $quarto[0] = "06 Quartos";};
                                    $icons = get_field('icones_blocos', 'option'); 
                                    $theid = get_the_ID();   
                                    $exibir_imoveis = get_field( 'pagina_sobre_imoveis', 'option' ); 
                        ?>          
                        <?php if ($fase_da_obra[0] == $exibir_imoveis){ ?>
                            <style>
                                .image_results_controller._<?php echo $theid; ?>{
                                    background-image:url(<?= the_post_thumbnail_url(); ?>);
                                }    
                            </style>

                            <div id="loop-emp" class="swiper-slide results-list">
                                <div class=" search-filter-result-item post">
                                <a href="<?php the_permalink(); ?>">
                                        <div class="image_results_controller _<?php echo $theid; ?>"></div>
                                        <?php if($cidade[0] && $estado[0]) {?>
                                        <div class="result_content_controller">
                                            <?php if(get_field('residencial')){ ?><p><?php echo get_field('residencial') ?></p><?php }else{} ?>
                                            <?php if($cidade[0] && $estado[0]) { ?><h3><?= $cidade[0]; ?><span><?= $estado[0]; ?></span></h3> <?php } else {} ?>
                                            <?php if(get_field('condominio')) { ?><p> Condomínio <?php echo get_field('condominio') ?></p><?php } else {} ?>
                                            <?php if($fase_da_obra[0] or get_field('itbi_info')){?>
                                                <div class="tags">
                                                    <?php if($fase_da_obra[0] == "Lançamento"){$fase_da_obra[0] = "Lancamento";}else if($fase_da_obra[0] == "Breve Lançamento") { $fase_da_obra[0] = "Breve_Lancamento";} else if($fase_da_obra[0] == "Documentação Grátis") { $fase_da_obra[0] = "Documentacao_gratis";} else {};  ?>
                                                    <?php if($fase_da_obra[0]) { ?><p class="tag on <?= $fase_da_obra[0] ?>"><?php if($fase_da_obra[0] == "Lancamento"){echo $fase_da_obra[0] = "Lançamento";} else if($fase_da_obra[0] == "Breve_Lancamento"){echo $fase_da_obra[0] = "Breve Lançamento";} else if($fase_da_obra[0] == "Documentacao_gratis") { $fase_da_obra[0] = "Documentação Grátis";} else { echo $fase_da_obra[0]; }; ?></p> <?php } else {} ?>
                                                    <?php if(get_field('itbi_info')) { ?><p class="tag off <?php if( get_field('itbi_info') == "Documentação Grátis" ) { echo "Documentacao_gratis"; } else {  echo get_field('itbi_info');  } ?>"><?php echo get_field('itbi_info'); ?></p> <?php } else {} ?>
                                                </div>
                                            <?php }else{} ?>
                                        
                                            <?php
                                                if($quarto[0]){
                                                while( have_rows('icones_blocos', 'options') ): the_row(); 
                                            ?>
                                            <style>
                                                

                                                .icon.icon-moby{
                                                    background-image:url(<?php echo get_sub_field('quartos_icon')['url'];?>);
                                                }
                                                .icon.icon-req{
                                                    background-image:url(<?php echo get_sub_field('medidas_icon')['url'];?>);
                                                }
                                            </style>
                                            <?php
                                                endwhile;
                                            }else{}
                                            ?>

                                            <?php if($quarto[0]){?><p><i class="icon icon-moby"></i><?= $quarto[0]; ?> </p> <?php } else {} ?>

                                            <?php if(get_field('medidas') || get_field('medidas_2')){ ?>
                                                <div class="medidas">
                                                    <p class="m2"><i class="icon icon-req"></i><?php echo get_field('medidas') ?> m</p>
                                                    <?php if(get_field('medidas_2')){?><span class="m2 left">a <?php echo get_field('medidas_2') ?> m</span><?php }else{}?> 
                                                </div>
                                            <?php } else {}?>
                                            
                                        </div>
                                            <?php } else { echo "preencha, cidade e estado."; } ?>
                                    </a>
                                </div>
                            </div>
                        <?php  } else {} ?>
                        

                <?php 
                    endforeach;
                    wp_reset_postdata();

                ?>
            </div>
    </section> 
    </section>
<?php

    }
?>






        
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
        </div>
   </section>
    <?php
    //get_template_part('template_part/components/page_about_cac/launch_imv');

get_footer(); ?>


<script>

$(window).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
            $('#header').addClass('sticky');
            $('.mobile-nav-wrap').addClass('sticky');
        } else {
            $('#header').removeClass('sticky');
            $('.mobile-nav-wrap').removeClass('sticky');
        }
    });

    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
            //$('#header').addClass('sticky');
            $('.mobile-nav-wrap').addClass('sticky');
        } else {
            //$('#header').removeClass('sticky');
            $('.mobile-nav-wrap').removeClass('sticky');
        }
    });

var toggleLevel = function(e){
	var l = e.data.level;
	var $menu = $('menu');
	if(l == 1){
		if($(e.target).hasClass('js-menu') || ($('.is-open').length && !$(e.target).parents('nav').length))
		$menu.toggleClass('is-open');
	}
	else if(l == 2){
		if($menu.hasClass('is-first-level')){
			$('.second-level, menu').removeClass('is-second-level is-first-level');
		}
		else{
			$menu.addClass('is-first-level').find($(this)).next('.second-level').addClass('is-second-level');
		}
	}
}

$('.first-level li > a, .back').on("click", { level: 2 }, toggleLevel);

$(document).on('click', { level: 1 }, toggleLevel);


// open mobile menu
$('.js-toggle-menu').click(function(e){
    e.preventDefault();
    $('.mobile-header-nav').slideToggle();
    $(this).toggleClass('open');
});
</script>