<?php 
/*
* Template Name: Obrigado
* Template para página de Obrigado
*/
get_header();

    get_template_part('template_part/components/tab_info');
    //search imoveis
    //get_template_part('template_part/components/page_contact/main_banner');
    //get_template_part('template_part/components/page_contact/section_contact');
    ?>

    <div class="the-content">
        <?php  echo the_content(); ?>
    </div>

    <section class="full-container-no-bg">
        <h4>Veja outros imóveis semelhantes a esse!</h4>
        <div class="swiper-container launch">
            <?php get_template_part('/template_part/components/show_launch'); ?>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </section>

    <?php

get_footer(); ?>

