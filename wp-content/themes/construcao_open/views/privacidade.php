<link rel="stylesheet" href="https://www.cacengenharia.com.br/wp-content/themes/construcao/build/css/home.min.css" type="text/css" media="all">
<script type="text/javascript" src="https://www.cacengenharia.com.br/wp-content/themes/construcao/build/js-lib/home-lib.min.js"></script>
<script type="text/javascript" src="https://www.cacengenharia.com.br/wp-content/themes/construcao/build/js/home.min.js"></script>


<?php 
/*
* Template Name: Privacidade
* Template para página de Privacidade
*/
get_header();

    get_template_part('template_part/components/tab_info');
    //search imoveis
    //get_template_part('template_part/components/page_contact/main_banner');
    //get_template_part('template_part/components/page_contact/section_contact');
    ?>

    <section class="polices">
    <div class="the-content">
        <?php  echo the_content(); ?>
    </div>
    </section>

    <?php

get_footer(); ?>
