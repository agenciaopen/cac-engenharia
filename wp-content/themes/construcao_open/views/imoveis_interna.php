<?php get_template_part('template_part/variables/variables'); ?>

<section class="has-container">
    <style>
        #crumbs:before {
            background-image: url(<?php echo get_field('icone_breadcrumb', 'option');
                                    ?>);
            content: '';
            display: block;
            width: 1.25em;
            height: 1.25em;
            background-size: 100%;
            background-repeat: no-repeat;
            float: left;
            margin: 0 1% 0 0;
        }
    </style>
    <?php wp_custom_breadcrumbs(); ?>

    <?php
    $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
    $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
    $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
    $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));

    if ($fase_da_obra[0] == "Breve Lançamento") { ?>
    <?php } else { ?>

        <?php if ($fase_da_obra[0] or get_field('itbi_info') or get_field('condominio')) { ?>
            <h1 class="title_imv"><?php echo get_field('titulo_pagina'); ?></h1>
            <div class="tags">
                <?php if ($fase_da_obra[0]) { ?><p class="tag interna <?= $fase_da_obra[0] ?>">
                        <?php if ($fase_da_obra[0] == "Lancamento") {
                            echo $fase_da_obra[0] = "Lançamento";
                        } else {
                            echo $fase_da_obra[0];
                        }; ?>
                    </p> <?php } else {
                        } ?>
                <?php if (get_field('condominio')) { ?><p class="tag interna <?php echo get_field('condominio'); ?>">Condomínio
                        <?php echo get_field('condominio'); ?></p> <?php } else {
                                                                } ?>
                <?php if (get_field('tag_interna')) { ?><p class="tag interna <?php echo get_field('tag_interna'); ?>">
                        <?php echo get_field('tag_interna'); ?></p> <?php } else {
                                                                } ?>
                <?php while (have_rows('tags_imovel')) : the_row(); ?>
                    <?php if (get_sub_field('tag_1') or get_sub_field('tag_2') or get_sub_field('tag_3') or get_sub_field('tag_4') or get_sub_field('tag_5')) { ?>
                        <?php if (get_sub_field('tag_1')) { ?><p class="tag interna <?php echo get_sub_field('tag_1'); ?>">
                                <?php echo get_sub_field('tag_1'); ?></p><?php } else {
                                                                        } ?>
                        <?php if (get_sub_field('tag_2')) { ?><p class="tag interna <?php echo get_sub_field('tag_2'); ?>">
                                <?php echo get_sub_field('tag_2'); ?></p><?php } else {
                                                                        } ?>
                        <?php if (get_sub_field('tag_3')) { ?><p class="tag interna <?php echo get_sub_field('tag_3'); ?>">
                                <?php echo get_sub_field('tag_3'); ?></p><?php } else {
                                                                        } ?>
                        <?php if (get_sub_field('tag_4')) { ?><p class="tag interna <?php echo get_sub_field('tag_4'); ?>">
                                <?php echo get_sub_field('tag_4'); ?></p><?php } else {
                                                                        } ?>
                        <?php if (get_sub_field('tag_5')) { ?><p class="tag interna <?php echo get_sub_field('tag_5'); ?>">
                                <?php echo get_sub_field('tag_5'); ?></p><?php } else {
                                                                        } ?>
                    <?php } else {
                    } ?>
                <?php endwhile; ?>
                <?php if (get_field('itbi_info')) { ?><p class="tag interna <?php echo get_field('itbi_info'); ?>">
                        <?php echo get_field('itbi_info'); ?></p> <?php } else {
                                                                } ?>
            </div>
        <?php } else {
        } ?>

    <?php } ?>
</section>

<section class="menu-interno">

    <a href="#sobreImovel">
        <p >Sobre o imóvel</p>
    </a>
    
    <a href="#diferenciaisImovel">
        <p >Diferenciais</p>
    </a>
    <a href="#imagensImovel">
        <p>Galeria de imagens</p>
    </a>
    <a href="#localizacaoImovel">
        <p>Localização</p>
    </a>

</section>


<?php
get_template_part('template_part/components/tab_info');
//components page
get_template_part('template_part/components/page_intern_imv/section_about_imv');
if ($fase_da_obra[0] == "Breve Lançamento") {
    get_template_part('template_part/components/page_intern_imv/progress_bar');
} else {
    get_template_part('template_part/components/page_intern_imv/section_differentials');
    get_template_part('template_part/components/page_intern_imv/image_gallery');
    get_template_part('template_part/components/page_intern_imv/progress_bar');
    get_template_part('template_part/components/page_intern_imv/map_in_imv');
}
get_template_part('template_part/components/page_intern_imv/section_cta_imv');
//get_template_part('template_part/components/page_intern_imv/view_fixed_imv_suggest');
?>

<input type="hidden" id="codempreendimento" value="<?php echo get_field('codempreendimento'); ?>">

<section class="full-container-no-bg">
    <h4>Veja outros imóveis semelhantes a esse!</h4>
    <div class="swiper-container launch">
        <?php
        $semelhante = $estado[0];

        $imv_posts = get_posts(
            array(
                'posts_per_page' => -1,
                'offset'         => 0,
                'post_type'         => 'imoveis',
                'orderby'        => 'cat',
                'order'          => 'DSC'
            )
        );

        if ($imv_posts) { ?>
            <section id="if_moby">
                <section class="resulting launch">
                    <div class="swiper-wrapper content-loop">
                        <?php
                        foreach ($imv_posts as $post) :
                            setup_postdata($post);
                            $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
                            $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
                            $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
                            $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
                            $faixa_de_preco = wp_get_post_terms($post->ID, 'faixa_de_preco', array("fields" => "names"));
                            if ($quarto[0] == "01") {
                                $quarto[0] = "01 Quarto";
                            } else if ($quarto[0] == "02") {
                                $quarto[0] = "02 Quartos";
                            } else if ($quarto[0] == "03") {
                                $quarto[0] = "03 Quartos";
                            } else if ($quarto[0] == "04") {
                                $quarto[0] = "04 Quartos";
                            } else if ($quarto[0] == "05") {
                                $quarto[0] = "05 Quartos";
                            } else if ($quarto[0] == "06") {
                                $quarto[0] = "06 Quartos";
                            };
                            $icons = get_field('icones_blocos', 'option');
                            $theid = get_the_ID();
                        ?>
                            <?php if ($estado[0] == $semelhante) { ?>
                                <style>
                                    .image_results_controller._<?php echo $theid; ?> {
                                        background-image: url(<?= the_post_thumbnail_url(); ?>);
                                    }
                                </style>

                                <div id="loop-emp" class="swiper-slide results-list">
                                    <div class=" search-filter-result-item post">
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="image_results_controller _<?php echo $theid; ?>"></div>
                                            <?php if ($cidade[0] && $estado[0]) { ?>
                                                <div class="result_content_controller">
                                                    <?php if (get_field('residencial')) { ?><p><?php echo get_field('residencial') ?></p><?php } else {
                                                                                                                                        } ?>
                                                    <?php if ($cidade[0] && $estado[0]) { ?><h3><?= $cidade[0]; ?><span><?= $estado[0]; ?></span></h3> <?php } else {
                                                                                                                                                    } ?>
                                                    <?php if (get_field('condominio')) { ?><p> Condomínio <?php echo get_field('condominio') ?></p><?php } else {
                                                                                                                                                } ?>
                                                    <?php if ($fase_da_obra[0] or get_field('itbi_info')) { ?>
                                                        <div class="tags">
                                                            <?php if ($fase_da_obra[0] == "Lançamento") {
                                                                $fase_da_obra[0] = "Lancamento";
                                                            } else if ($fase_da_obra[0] == "Breve Lançamento") {
                                                                $fase_da_obra[0] = "Breve_Lancamento";
                                                            } else if ($fase_da_obra[0] == "Documentação Grátis") {
                                                                $fase_da_obra[0] = "Documentacao_gratis";
                                                            } else {
                                                            };  ?>
                                                            <?php if ($fase_da_obra[0]) { ?><p class="tag on <?= $fase_da_obra[0] ?>"><?php if ($fase_da_obra[0] == "Lancamento") {
                                                                                                                                            echo $fase_da_obra[0] = "Lançamento";
                                                                                                                                        } else if ($fase_da_obra[0] == "Breve_Lancamento") {
                                                                                                                                            echo $fase_da_obra[0] = "Breve Lançamento";
                                                                                                                                        } else if ($fase_da_obra[0] == "Documentacao_gratis") {
                                                                                                                                            $fase_da_obra[0] = "Documentação Grátis";
                                                                                                                                        } else {
                                                                                                                                            echo $fase_da_obra[0];
                                                                                                                                        }; ?></p> <?php } else {
                                                                                                                                                } ?>
                                                            <?php if (get_field('itbi_info')) { ?><p class="tag off <?php if (get_field('itbi_info') == "Documentação Grátis") {
                                                                                                                        echo "Documentacao_gratis";
                                                                                                                    } else {
                                                                                                                        echo get_field('itbi_info');
                                                                                                                    } ?>"><?php echo get_field('itbi_info'); ?></p> <?php } else {
                                                                                                                                                                } ?>
                                                        </div>
                                                    <?php } else {
                                                    } ?>

                                                    <?php
                                                    if ($quarto[0]) {
                                                        while (have_rows('icones_blocos', 'options')) : the_row();
                                                    ?>
                                                            <style>
                                                                .icon.icon-moby {
                                                                    background-image: url(<?php echo get_sub_field('quartos_icon')['url']; ?>);
                                                                }

                                                                .icon.icon-req {
                                                                    background-image: url(<?php echo get_sub_field('medidas_icon')['url']; ?>);
                                                                }
                                                            </style>
                                                    <?php
                                                        endwhile;
                                                    } else {
                                                    }
                                                    ?>

                                                    <?php if ($quarto[0]) { ?><p><i class="icon icon-moby"></i><?= $quarto[0]; ?> </p> <?php } else {
                                                                                                                                    } ?>

                                                    <?php if (get_field('medidas') || get_field('medidas_2')) { ?>
                                                        <div class="medidas">
                                                            <p class="m2"><i class="icon icon-req"></i><?php echo get_field('medidas') ?> m</p>
                                                            <?php if (get_field('medidas_2')) { ?><span class="m2 left">a <?php echo get_field('medidas_2') ?> m</span><?php } else {
                                                                                                                                                                    } ?>
                                                        </div>
                                                    <?php } else {
                                                    } ?>

                                                </div>
                                            <?php } else {
                                                echo "preencha, cidade e estado.";
                                            } ?>
                                        </a>
                                    </div>
                                </div>
                            <?php  } else {
                            } ?>


                        <?php
                        endforeach;
                        wp_reset_postdata();

                        ?>
                    </div>
                </section>
            </section>
        <?php

        }
        ?>
        <div class="swiper-button-prev launch"></div>
        <div class="swiper-button-next launch"></div>
    </div>
</section>



<?php if (is_single()) { ?>
    <script src="<?php echo get_template_directory_uri() . '/build/js/imask.min.js'; ?>"></script>
    <script src="<?php echo get_template_directory_uri() . '/build/js/single.js'; ?>"></script>

    <script>
        $(window).on('scroll', function() {
            if ($(window).scrollTop() > 100) {
                //$('#header').addClass('sticky');
                $('.mobile-nav-wrap').addClass('sticky');
            } else {
                //$('#header').removeClass('sticky');
                $('.mobile-nav-wrap').removeClass('sticky');
            }
        });

        $(window).on('scroll', function() {
            if ($(window).scrollTop() > 100) {
                $('#header').addClass('sticky');
                $('.mobile-nav-wrap').addClass('sticky');
            } else {
                $('#header').removeClass('sticky');
                $('.mobile-nav-wrap').removeClass('sticky');
            }
        });


        var toggleLevel = function(e) {
            var l = e.data.level;
            var $menu = $('menu');
            if (l == 1) {
                if ($(e.target).hasClass('js-menu') || ($('.is-open').length && !$(e.target).parents('nav').length))
                    $menu.toggleClass('is-open');
            } else if (l == 2) {
                if ($menu.hasClass('is-first-level')) {
                    $('.second-level, menu').removeClass('is-second-level is-first-level');
                } else {
                    $menu.addClass('is-first-level').find($(this)).next('.second-level').addClass('is-second-level');
                }
            }
        }

        $('.first-level li > a, .back').on("click", {
            level: 2
        }, toggleLevel);

        $(document).on('click', {
            level: 1
        }, toggleLevel);


        // open mobile menu
        $('.js-toggle-menu').click(function(e) {
            e.preventDefault();
            $('.mobile-header-nav').slideToggle();
            $(this).toggleClass('open');
        });




        $(window).resize(function() {
            var now_w = $(window).width();

            if (now_w >= 1920) {
                $('.bg_').removeClass('moby');
            } else if (now_w <= 768) {
                $('.bg_ ').addClass('moby');
            }
        });


        $(document).ready(function() {
            var now_w = $(window).width();

            if (now_w >= 1920) {
                $('.bg_ ').removeClass('moby');
            } else if (now_w <= 768) {
                $('.bg_ ').addClass('moby');
            }
        });
    </script>

<?php } else {
} ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/imask/5.2.1/imask.min.js" integrity="sha256-xj2lSOVYUEOObcK2OI0cA7XiLDn0TgBzWeVcQuLcMtw=" crossorigin="anonymous"></script>

<script>
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 100) {
            //$('#header').addClass('sticky');
            $('.mobile-nav-wrap').addClass('sticky');
        } else {
            //$('#header').removeClass('sticky');
            $('.mobile-nav-wrap').removeClass('sticky');
        }
    });

    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 100) {
            $('#header').addClass('sticky');
            $('.mobile-nav-wrap').addClass('sticky');
        } else {
            $('#header').removeClass('sticky');
            $('.mobile-nav-wrap').removeClass('sticky');
        }
    });

    $(window).resize(function() {
        var now_w = $(window).width();

        if (now_w >= 1920) {
            $('.bg_').removeClass('moby');
        } else if (now_w <= 768) {
            $('.bg_ ').addClass('moby');
        }
    });


    $(document).ready(function() {
        var now_w = $(window).width();

        if (now_w >= 1920) {
            $('.bg_ ').removeClass('moby');
        } else if (now_w <= 768) {
            $('.bg_ ').addClass('moby');
        }
    });

    var toggleLevel = function(e) {
        var l = e.data.level;
        var $menu = $('menu');
        if (l == 1) {
            if ($(e.target).hasClass('js-menu') || ($('.is-open').length && !$(e.target).parents('nav').length))
                $menu.toggleClass('is-open');
        } else if (l == 2) {
            if ($menu.hasClass('is-first-level')) {
                $('.second-level, menu').removeClass('is-second-level is-first-level');
            } else {
                $menu.addClass('is-first-level').find($(this)).next('.second-level').addClass('is-second-level');
            }
        }
    }

    $('.first-level li > a, .back').on("click", {
        level: 2
    }, toggleLevel);

    $(document).on('click', {
        level: 1
    }, toggleLevel);


    // open mobile menu
    $('.js-toggle-menu').click(function(e) {
        e.preventDefault();
        $('.mobile-header-nav').slideToggle();
        $(this).toggleClass('open');
    });
</script>