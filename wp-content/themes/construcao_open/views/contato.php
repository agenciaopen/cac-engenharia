<?php
/*
* Template Name: Contato
* Template para página de Contato
*/
get_header();

get_template_part('template_part/components/tab_info');
//search imoveis
?>
<?php

$banner_contato_mobile = get_field('banner_mobile_c', $page_ID);
$banner_contato_desktop = get_field('banner_esquerda', $page_ID);
?>


<section class="">
    <img src="<?php echo $banner_contato_mobile; ?>" alt="<?php echo $page_ID; ?>" title="" loading="lazy" srcset="" class="contato__banner--mob" />
    <img src="<?php echo $banner_contato_desktop; ?>" alt="<?php echo $page_ID; ?>" title="" loading="lazy" srcset="" class="contato__banner--desktop" />
</section>
<?php
get_template_part('template_part/components/page_contact/section_contact'); ?>

<section class="full-container-no-bg">
    <div class="swiper-container launch">
        <?php get_template_part('template_part/layout/imoveis-fixed-with-pagination'); ?>

        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</section>

<?php

get_footer(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/build/js/scripts.min.js'; ?>"></script>