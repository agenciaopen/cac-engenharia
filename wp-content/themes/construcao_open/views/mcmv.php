<?php 
/*
* Template Name: MCMV
* Template para página de Minha casa minha vida
*/
get_header();

    get_template_part('template_part/components/tab_info');
    //search imoveis
    get_template_part('template_part/components/page_mcmv/main_banner');
  
        //components page
        get_template_part('template_part/components/page_mcmv/section_mcmv');
     
    
    ?>

    <?php

get_footer(); ?>