var url = window.location.protocol + "//" + window.location.host;

var viewport = {
    w: $(window).width(),
    h: $(window).height()
};

$(window).resize(function () {
    viewport = {
        w: $(window).width(),
        h: $(window).height()
    };
});

var isMobile = function () {
    return (viewport.w < 768); 
};

var isTablet = function () {
    return (viewport.w >= 768 && viewport.w <= 1024);
};

var isDesktop = function () { 
    return (viewport.w > 1024);
};

function msieVersion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0) /* If Internet Explorer, return version number */
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    else /* If another browser, return 0 */
        return 0;
}
function ie() {
    var ua = window.navigator.userAgent,
      msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    return false;
}

function classPage( $page ){
    $('.menu').find('.' + $page ).addClass('ativo');
}

function ancora(){
    var off;
    var $doc = $('html, body');
    $('.ancora').click(function() {
        
        off = $(this).data('ancora') ? $(this).data('ancora') : 0;

        $doc.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top - off
        }, 800);
        return false;
    });
} 


function video() {
    var boxVideo = $(".youtube_video");

    boxVideo.each(function () {
        var idVideo = $(this).attr('href');
        var str = idVideo;
        var res = str.replace("https://www.youtube.com/embed/", "");

        var urlImage = "https://img.youtube.com/vi/" + res + "/hqdefault.jpg";
        var img = new Image();
        img.src = urlImage;
        img.alt = $(this).attr('title');
        $(this).find('.img').append(img);

    });

}

function sliderSingle( obeject ) {
    $('.'+obeject).slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        speed: 500,
        fade: true,
        cssEase: 'linear', 
        autoplay: true,
        autoplaySpeed: 10000
    });
}

function doAnimations(elements) {
    var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    elements.each(function() {
        var $this = $(this);
        var $animationDelay = $this.data('delay');
        var $animationType = 'animated ' + $this.data('animation');
        $this.css({
            'animation-delay': $animationDelay,
            '-webkit-animation-delay': $animationDelay
        });
        $this.addClass($animationType).one(animationEndEvents, function() {
            $this.removeClass($animationType);
        });
    });
}