
  $(document).ready(function () {
    var banner_principal = new Swiper('.banner', {
        loop:false,
        direction: 'vertical',
        cssMode: true,
        mousewheel: true,
        keyboard: true,
      });
  });
  
  var search_imoveis_ = document.location.href;
  console.log(search_imoveis_);

  if(search_imoveis_.includes('/minha-casa-minha-vida')){
    try{
        //$('.swiper-wrapper.banner_controller').html('');
        //$('.swiper-wrapper').removeClass('banner_controller');
        //$('.swiper-wrapper').removeClass('swiper-wrapper');
        $('.banner-category').html('');
        $('.banner-category').removeClass('banner-category');
    } catch(err){
        console.log('verificador href falhou, home.js - l:44');
    }
  }

  jQuery(document).ready(function($){

    var blocos_imoveis = new Swiper('.results', {
			slidesPerView: 4,
			slidesPerGroup: 4,
			spaceBetween: 45,
			loop:false,
			breakpoints: {
				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				  },
				640: {
				  slidesPerView: 1,
				  spaceBetween: 20,
				},
				768: {
				  slidesPerView: 2,
				  spaceBetween: 30,
				},
				1024: {
				  slidesPerView: 3,
				  spaceBetween: 45,
				},
				1366: {
                    slidesPerView: 4,
                    spaceBetween: 45,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			pagination: {
			  el: '.swiper-pagination',
			  clickable: true,
			}
      });
  });