


  var tab_caller = function (){
      var tabs = document.getElementById('icetab-container').children;
      var tabcontents = document.getElementById('icetab-content').children;
    
      var tabFunction = function() {
      var tabchange = this.mynum;
      for(var int=0;int<tabcontents.length;int++){
        tabcontents[int].className = ' tabcontent';
        tabs[int].className = ' icetab';
      }
        tabcontents[tabchange].classList.add('tab-active');
        this.classList.add('current-tab');
      }	
    
    
    for(var index=0;index<tabs.length;index++){
      tabs[index].mynum=index;
      tabs[index].addEventListener('click', tabFunction, false);
    }
  }
  tab_caller();

  var video_caller = function (){
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
      var get_vdId = document.getElementById('vdID').value;
      
      var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
    
      var get_thumbnail = document.getElementById('thumbnail').innerHTML += '<img src="https://img.youtube.com/vi/'+str_t+'/maxresdefault.jpg" alt="">';
     
      var player;
      function onYouTubePlayerAPIReady() {
          player = new YT.Player('ytplayer', {
          height: '100%',
          width: '100%',
          playerVars: {rel: 0, controls:0, iv_load_policy:3, modestbranding:0},
          videoId: str_t,
              events: {
                  'onReady': onPlayerReady,
                  'onStateChange': onPlayerStateChange
              }
          });
          
          function onPlayerStateChange(event) {
              
          }
    
          function onPlayerReady(event) {
              
              var effect_thumbnail = document.getElementById('thumbnail');
              var playButton = document.getElementById("play-button");
              var pauseButton = document.getElementById("pause-button");
    
              playButton.addEventListener("click", function() {
                pauseButton.classList.toggle("effect_show");
                playButton.classList.toggle("effect_hide");
                effect_thumbnail.classList.toggle("thumbnail_effect");
                player.playVideo();
              });
           
              pauseButton.addEventListener("click", function() {
                pauseButton.classList.toggle("effect_show");
                playButton.classList.toggle("effect_hide");
                effect_thumbnail.classList.toggle("thumbnail_effect");
                player.stopVideo();
              });
            }
      }
    } 
 


  jQuery(document).ready(function($){

    var blocos_imoveis = new Swiper('.results', {
			slidesPerView: 4,
			slidesPerGroup: 4,
			spaceBetween: 45,
			loop:false,
			breakpoints: {
				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				  },
				640: {
				  slidesPerView: 1,
				  spaceBetween: 20,
				},
				768: {
				  slidesPerView: 2,
				  spaceBetween: 30,
				},
				1024: {
				  slidesPerView: 3,
				  spaceBetween: 45,
				},
				1366: {
                    slidesPerView: 4,
                    spaceBetween: 45,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			pagination: {
			  el: '.swiper-pagination',
			  clickable: true,
			}
		  });

    var galleryThumbs = new Swiper('.gallery-thumbs-1', {
      spaceBetween: 4,
      slidesPerView: 4,
      loop: false,
      freeMode: false,
      loopedSlides: 1, //looped slides should be the same
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top-1', {
      spaceBetween: 4,
      loop:true,
      loopedSlides: 4, //looped slides should be the same
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs,
      },
    });

    var galleryThumbs2 = new Swiper('.gallery-thumbs-2', {
      spaceBetween: 4,
      slidesPerView: 4,
      loop: false,
      freeMode: false,
      loopedSlides: 1, //looped slides should be the same
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });
    var galleryTop2 = new Swiper('.gallery-top-2', {
      spaceBetween: 4,
      loop:true,
      loopedSlides: 4, //looped slides should be the same
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs2,
      },
    });

    var galleryThumbs3 = new Swiper('.gallery-thumbs-3', {
      spaceBetween: 4,
      slidesPerView: 4,
      loop: false,
      freeMode: false,
      loopedSlides: 1, //looped slides should be the same
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });
    var galleryTop3 = new Swiper('.gallery-top-3', {
      spaceBetween: 4,
      loop:true,
      loopedSlides: 4, //looped slides should be the same
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs3,
      },
    });

  });

  jQuery(document).ready(function($){
    moveProgressBar();
    $(window).resize(function() {
        moveProgressBar();
    });

    function moveProgressBar() {
        var getPercent = ($('.progress-wrap').data('progress-percent') / 100);
        var getProgressWrapWidth = $('.progress-wrap').width();
        var progressTotal = getPercent * getProgressWrapWidth;
        var animationLength = 2500;

        $('.progress-bar').stop().animate({
            left: progressTotal
        }, animationLength);
    }
  });

  (function( $ ) {

    /**
     * initMap
     *
     * Renders a Google Map onto the selected jQuery element
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   jQuery $el The jQuery element.
     * @return  object The map instance.
     */
    function initMap( $el ) {
    
        // Find marker elements within map.
        var $markers = $el.find('.marker');
    
        // Create gerenic map.
        var mapArgs = {
            zoom        : $el.data('zoom') || 16,
            mapTypeId   : google.maps.MapTypeId.ROADMAP,
            styles: [
              {
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#f5f5f5"
                  }
                ]
              },
              {
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#616161"
                  }
                ]
              },
              {
                "elementType": "labels.text.stroke",
                "stylers": [
                  {
                    "color": "#f5f5f5"
                  }
                ]
              },
              {
                "featureType": "administrative.land_parcel",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#bdbdbd"
                  }
                ]
              },
              {
                "featureType": "administrative.neighborhood",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#eeeeee"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#757575"
                  }
                ]
              },
              {
                "featureType": "poi.business",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#e5e5e5"
                  }
                ]
              },
              {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#9e9e9e"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#ffffff"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#757575"
                  }
                ]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#dadada"
                  }
                ]
              },
              {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#616161"
                  }
                ]
              },
              {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#9e9e9e"
                  }
                ]
              },
              {
                "featureType": "transit",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#e5e5e5"
                  }
                ]
              },
              {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#eeeeee"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#c9c9c9"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#9e9e9e"
                  }
                ]
              }
            ],
        };
        var map = new google.maps.Map( $el[0], mapArgs );

        // Add markers.
        map.markers = [];
        $markers.each(function(){
            initMarker( $(this), map );

        });

        
    
        // Center map based on markers.
        centerMap( map );
    
        // Return map instance.
        return map;
    }
    
    /**
     * initMarker
     *
     * Creates a marker for the given jQuery element and map.
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   jQuery $el The jQuery element.
     * @param   object The map instance.
     * @return  object The marker instance.
     */
    function initMarker( $marker, map ) {
    
        // Get position from marker.
        var lat = $marker.data('lat');
        var lng = $marker.data('lng');
        var latLng = {
            lat: parseFloat( lat ),
            lng: parseFloat( lng )
        };
    
        var iconBase =
        '/wp-content/themes/construcao/config/src/';
        

        // Create marker instance.
        var marker = new google.maps.Marker({
            position : latLng,
            map: map,
            icon: iconBase + 'marker.svg'
        });
    
        // Append to reference for later use.
        map.markers.push( marker );
    
        // If marker contains HTML, add it to an infoWindow.
        if( $marker.html() ){
    
            // Create info window.
            var infowindow = new google.maps.InfoWindow({
                content: $marker.html()
            });
    
            // Show info window when marker is clicked.
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open( map, marker );
            });
        }
    }
    
    /**
     * centerMap
     *
     * Centers the map showing all markers in view.
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   object The map instance.
     * @return  void
     */
    function centerMap( map ) {
    
        // Create map boundaries from all map markers.
        var bounds = new google.maps.LatLngBounds();
        map.markers.forEach(function( marker ){
            bounds.extend({
                lat: marker.position.lat(),
                lng: marker.position.lng()
            });
        });
    
        // Case: Single marker.
        if( map.markers.length == 1 ){
            map.setCenter( bounds.getCenter() );
    
        // Case: Multiple markers.
        } else{
            map.fitBounds( bounds );
        }
    }
    
    // Render maps on page load.
    $(document).ready(function(){
        $('.acf-map').each(function(){
            var map = initMap( $(this) );
        });
    });
    
    })(jQuery);
    
   
 
var getroute_empreendimento = function(){
  var call_route = document.getElementById('calcular_rota');
  var get_route_e = document.getElementById('CEP_empreendimento');

  get_route_e.addEventListener('change', function(){
    console.log(get_route_e.value);
  });
  
  var err_em = document.getElementById('err_em');
  err_em.innerHTML += '';
  
  call_route.addEventListener('click', function(){
    var get_route_go_e = document.getElementById('empreendimento_rota').value;
    var str_route = "https://www.google.com.br/maps/dir/"+get_route_e.value+"/"+get_route_go_e+"/";
    if(get_route_e.value == ''){
        var err_em = document.getElementById('err_em');
        err_em.classList.toggle('active');
        if(err_em.classList.contains('active') == true){
          err_em.innerText = "O cálculo da rota só pode ser feito com o CEP";
        } else if(err_em.classList.contains('active') == false){
          err_em.innerText = "";
        }
    }else{
        window.open(str_route, "_blank");
        var err_em = document.getElementById('err_em');
        err_em.classList.toggle('active');
        err_em.innerText = "";
    }
  });
}


 
var getroute_estande = function(){
  var call_routes = document.getElementById('calcular_estande_rota');
  var get_route_es = document.getElementById('CEP_estande').value;

  call_routes.addEventListener('click', function(){
    var get_route_go_es = document.getElementById('estande_rota').value;
    var str_route = "https://www.google.com.br/maps/dir/"+get_route_es+"/"+get_route_go_es+"/";
    if(get_route_es == ''){
      var err_es = document.getElementById('err_es');
      err_es.classList.toggle('active');
      if(err_es.classList.contains('active') == true){
        err_es.innerText = "O cálculo da rota só pode ser feito com o CEP";
      } else if(err_es.classList.contains('active') == false){
        err_es.innerText = "";
      }
    }else{
      window.open(str_route, "_blank");
      var err_es = document.getElementById('err_es');
      err_es.classList.toggle('active');
      err_es.innerText = "";
    }
  });

}

var CEP_empreendimento = document.getElementById('CEP_empreendimento');
var CEP_estande = document.getElementById('CEP_estande');
var maskOptions = {
  mask: '00000-000'
};

var mask_em = IMask(CEP_empreendimento, maskOptions);
var mask_es = IMask(CEP_estande, maskOptions);


getroute_empreendimento();
getroute_estande();  
  