
  $(document).ready(function () {
    var banner_principal = new Swiper('.banner', {
        loop:false,
        direction: 'vertical',
        cssMode: true,
        mousewheel: true,
        keyboard: true,
      });
  });

  $(document).ready(function () {
     var estados = $('#estados-cliente');
        estados.on('change', function(){
            var es_val = estados.val();
            var if_mg = $('#if-mg');
            var if_rj = $('#if-rj');
            var if_sp = $('#if-sp');
            var if_es = $('#if-es');
            var if_df = $('#if-df');

            if(es_val == "MG"){
                if_mg.show();
                if_rj.hide();
                if_sp.hide();
                if_es.hide();
                if_df.hide();        
            } else if (es_val == "RJ"){
                if_mg.hide();
                if_rj.show();
                if_sp.hide();
                if_es.hide();
                if_df.hide();
            } else if (es_val == "SP"){
                if_mg.hide();
                if_rj.hide();
                if_sp.show();
                if_es.hide();
                if_df.hide();
            } else if (es_val == "ES"){
                if_mg.hide();
                if_rj.hide();
                if_sp.hide();
                if_es.show();
                if_df.hide();
            } else if (es_val == "DF"){
                if_mg.hide();
                if_rj.hide();
                if_sp.hide();
                if_es.hide();
                if_df.show();
            } else {
                console.log("Padrão");
            }
        });


        var estados_f = $('#estados-fornecedor');
        estados_f.on('change', function(){
            var es_val = estados_f.val();
            var if_mg = $('#if-mg-f');
            var if_rj = $('#if-rj-f');
            var if_sp = $('#if-sp-f');
            var if_es = $('#if-es-f');
            var if_df = $('#if-df-f');

            if(es_val == "MG"){
                if_mg.show();
                if_rj.hide();
                if_sp.hide();
                if_es.hide();
                if_df.hide();        
            } else if (es_val == "RJ"){
                if_mg.hide();
                if_rj.show();
                if_sp.hide();
                if_es.hide();
                if_df.hide();
            } else if (es_val == "SP"){
                if_mg.hide();
                if_rj.hide();
                if_sp.show();
                if_es.hide();
                if_df.hide();
            } else if (es_val == "ES"){
                if_mg.hide();
                if_rj.hide();
                if_sp.hide();
                if_es.show();
                if_df.hide();
            } else if (es_val == "DF"){
                if_mg.hide();
                if_rj.hide();
                if_sp.hide();
                if_es.hide();
                if_df.show();
            } else {
                console.log("Padrão");
            }
        });


        var estados_t = $('#estados-cliente-t');
        estados_t.on('change', function(){
            var es_val = estados_t.val();
            var if_mg = $('#if-mg-t');
            var if_rj = $('#if-rj-t');
            var if_sp = $('#if-sp-t');
            var if_es = $('#if-es-t');
            var if_df = $('#if-df-t');

            if(es_val == "MG"){
                if_mg.show();
                if_rj.hide();
                if_sp.hide();
                if_es.hide();
                if_df.hide();        
            } else if (es_val == "RJ"){
                if_mg.hide();
                if_rj.show();
                if_sp.hide();
                if_es.hide();
                if_df.hide();
            } else if (es_val == "SP"){
                if_mg.hide();
                if_rj.hide();
                if_sp.show();
                if_es.hide();
                if_df.hide();
            } else if (es_val == "ES"){
                if_mg.hide();
                if_rj.hide();
                if_sp.hide();
                if_es.show();
                if_df.hide();
            } else if (es_val == "DF"){
                if_mg.hide();
                if_rj.hide();
                if_sp.hide();
                if_es.hide();
                if_df.show();
            } else {
                console.log("Padrão");
            }
        });
  });


  var tab_caller = function (){
    var tabs = document.getElementById('icetab-container').children;
    var tabcontents = document.getElementById('icetab-content').children;
  
    var tabFunction = function() {
    var tabchange = this.mynum;
    for(var int=0;int<tabcontents.length;int++){
      tabcontents[int].className = ' tabcontent';
      tabs[int].className = ' icetab';
    }
      tabcontents[tabchange].classList.add('tab-active');
      this.classList.add('current-tab');
    }	
  
  
  for(var index=0;index<tabs.length;index++){
    tabs[index].mynum=index;
    tabs[index].addEventListener('click', tabFunction, false);
  }
}
tab_caller();
  
  var search_imoveis_ = document.location.href;
  console.log(search_imoveis_);

  if(search_imoveis_.includes('/contato')){
    try{
        //$('.swiper-wrapper.banner_controller').html('');
        //$('.swiper-wrapper').removeClass('banner_controller');
        //$('.swiper-wrapper').removeClass('swiper-wrapper');
        $('.banner-category').html('');
        $('.banner-category').removeClass('banner-category');
    } catch(err){
        console.log('verificador href falhou, home.js - l:44');
    }
  }

  jQuery(document).ready(function($){

    var blocos_imoveis = new Swiper('.results', {
			slidesPerView: 4,
			slidesPerGroup: 4,
			spaceBetween: 45,
			loop:false,
			breakpoints: {
				320: {
					slidesPerView: 1,
					spaceBetween: 0,
				  },
				640: {
				  slidesPerView: 1,
				  spaceBetween: 20,
				},
				768: {
				  slidesPerView: 2,
				  spaceBetween: 30,
				},
				1024: {
				  slidesPerView: 3,
				  spaceBetween: 45,
				},
				1366: {
                    slidesPerView: 4,
                    spaceBetween: 45,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			pagination: {
			  el: '.swiper-pagination',
			  clickable: true,
			}
      });
  });