(function () {
    if(document.getElementById('info_tab')){
        var footer = document.getElementById('footer');
        footer.classList.add('footer-tabbed');
   }

   $(window).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
            $('#header').addClass('sticky');
        } else {
            $('#header').removeClass('sticky');
        }
  });
})();   