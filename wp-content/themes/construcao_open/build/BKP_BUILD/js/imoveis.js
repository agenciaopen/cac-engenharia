(function () {
	jQuery(document).ready(function($){
		var $itemForm = $('.searchandfilter ul li');
		$itemForm.first().addClass( "ativo" );

		$( window ).on( "load", function() {
			$('.searchandfilter ul li').each(function (index, value) {
				var $valor = $( this ).find("select").val();
				if ( $valor ){
					$( this ).removeClass('ativo');
					$( this ).next().addClass('ativo');
				}
			});
		});


		$(document).on("sf:ajaxfinish", ".searchandfilter", function(){
			$('.searchandfilter ul li').each(function (index, value) {
				var $valor = $( this ).find("select").val();
				if ( $valor ){
					$( this ).removeClass('ativo');
					$( this ).next().addClass('ativo');
				}
			});
			$('.search-filter-result-item').each(function (index, value) {
				var $valor = $( index + '_posts' );

				console.log($valor.selector);

			});
		});
	});

	$(document).ready(function () {

		  var search_imoveis_ = document.location.href;

		  if (search_imoveis_.indexOf('/r-imoveis')) {
				console.log(search_imoveis_, true);
		 	}
		 

		  var search_val = $('.result .swiper-slide').length;

		  var search_imoveis = new Swiper('.result', {
			slidesPerView: search_val,
			slidesPerGroup: search_val,
			spaceBetween: 45,
			loop:false,
			breakpoints: {
				320: { 
					slidesPerView: 1,
					spaceBetween: 0,
				  },
				640: {
				  slidesPerView: 1,
				  spaceBetween: 20,
				},
				768: {
				  slidesPerView: 2,
				  spaceBetween: 30,
				},
				1024: {
				  slidesPerView: 3,
				  spaceBetween: 45,
				},
				1366: {
                    slidesPerView: 4,
                    spaceBetween: 45,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			pagination: {
			  el: '.swiper-pagination',
			  clickable: true,
			}
		  });


		  var launch_imoveis = new Swiper('.launch', {
			slidesPerView: 4,
			slidesPerGroup: 4,
			spaceBetween: 45,
			loop:false,
			breakpoints: {
				320: { 
					slidesPerView: 1,
					spaceBetween: 0,
				  },
				640: {
				  slidesPerView: 1,
				  spaceBetween: 20,
				},
				768: {
				  slidesPerView: 2,
				  spaceBetween: 30,
				},
				1024: {
				  slidesPerView: 3,
				  spaceBetween: 45,
				},
				1366: {
                    slidesPerView: 4,
                    spaceBetween: 45,
                },
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
			pagination: {
			  el: '.swiper-pagination',
			  clickable: true,
			}
		  });
	  });  
})();