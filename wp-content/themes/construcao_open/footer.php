            <?php
            $logo_principal = get_field('logo_footer', 'option');

            if (have_rows('secao_footer', 'options')) :
                while (have_rows('secao_footer', 'options')) : the_row();
            ?>
                    <style>
                        ._form_footer.bg_ {
                            background-image: url(<?php echo get_sub_field('footer_background'); ?>);
                        }
                    </style>
            <?php
                endwhile;
            endif;
            ?>

            <div id="pop-up-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body">
                            <p>Ultilizamos cookies e tecnologias semelhantes de acordo com a nossa <a href="https://www.cacengenharia.com.br/politica-de-privacidade/">Política de Privacidade</a> e, ao continuar navegando, você concorda com estas condiçoes.</p>
                        </div>
                        <div class="btn-modal">
                            <div class="modal-footer">
                                <button onclick="myFunction()" type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                            </div>
                            <div class="modal-footer">
                                <button onclick="myFunction()" type="button" class="btn btn-default" data-dismiss="modal">Não aceito</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <footer id="footer" class="hide-mobile">
                <section>



                    <div class="_form_infos">


                        <div class="_form_footer bg_">
                            <?php
                            if (have_rows('secao_footer', 'options')) :
                                while (have_rows('secao_footer', 'options')) : the_row();
                            ?>
                                    <h3><?php echo get_sub_field('footer_texto'); ?></h3>
                            <?php
                                endwhile;
                            endif;
                            ?>
                            <?= do_shortcode('[contact-form-7 id="183" title="cta footer"]'); ?>
                        </div>
                        <div class="_info_footer">
                            <div class="footer__ajuest">

                                <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>" alt="<?php bloginfo('name'); ?>">
                                    <img src="/wp-content/themes/construcao_open/assets/logocac.png" class="footer__logo" />
                                </a>

                                <?php
                                //redes sociais
                                get_template_part('template_part/components/social_media');
                                ?>

                            </div>
                            <div>
                            <div class="_info_controller">
                                <div class="pages_presentation">
                                    <h3>Acesso Rápido</h3>
                                    <ul class="footer__ul">
                                        <?php
                                        if (have_posts()) :
                                            while (have_posts()) : the_post();
                                            endwhile;
                                        ?>

                                            <?php $args = [
                                                'posts_per_page' => -1,
                                                'title_li' => '',
                                            ];
                                            wp_list_pages($args); ?>

                                        <?php
                                        else :
                                            echo '<p>There are no pages!</p>';
                                        endif;
                                        ?>
                                    </ul>

                                </div>
                                <div class="footer__div">
                                    <h3>
                                        Entre em contato e agende sua visita.
                                    </h3>
                                    <div class="two_in">

                                        <h4>Sede Administrativa</h4>
                                        <p class="footer__texto">Minas Gerais: <a href="tel:+55313063-0081">(31) 3063-0081</a>
                                            <br>
                                            Central de Vendas: <a href="tel:+550800 944 0081">0800 944 0081</a>
                                        </p>

                                        <p class="footer__texto">Rua Gabriela de Melo, 351
                                            <br>
                                            CEP 30.390-080 Bairro Olhos D'água - Belo Horizonte MG
                                        </p>


                                    </div>

                                </div>
                                <div class="footer__div">
                                    <h3>Assessoria de <br>Imprensa</h3>
                                    <p class="footer__texto"> Quadratto Comunicação. Rebecca Ramos: (21) 3942-2255 / <br>(21) 98102-8609 e rebecca@quadratto.com.br.
                                    </p>
                                    <p class="footer__texto">
                                        Proteção de dados - dpo@cacengenharia.com.br
                                    </p>
                                </div>

                            </div>
                            <p class="copy__desktop"> COPYRIGHT CAC ENGENHARIA LTDA 2014 – TODOS OS DIREITOS RESERVADOS.
                            </p>

                            </div>

                        </div>

                    </div>
                </section>
            </footer>

            <?php

            get_template_part('template_part/mobile_components/footer');
            wp_footer();

            ?>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/build/js-lib/' . add_file_min() . '-lib.min.js'; ?>"></script>
            <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/build/js/' . add_file_min() . '.min.js'; ?>"></script>
            <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
            <script src="https://cdn.botframework.com/botframework-webchat/latest/CognitiveServices.js"></script>
            <script src="https://cdn.botframework.com/botframework-webchat/master/botchat.js"></script>
            <?php if (is_page_template()) { ?> <?php } else if (is_single()) { ?> <?php } else { ?> <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/build/js/scripts.min.js'; ?>"></script> <?php } ?>

            <script>
                /*window.onload = function() {
  document.getElementById('pop-up-modal').className = 'show';
};*/

                function myFunction() {
                    var element = document.getElementById("pop-up-modal");
                    element.classList.remove("show");
                    element.classList.add("hide");
                }



                $(document).ready(function() {
                    if ($.cookie("popup_1_2") == null) {
                        $('#pop-up-modal').modal('show');
                        $.cookie("popup_1_2", "2");
                    }
                });
            </script>





            <script type="text/javascript">
                if (document.getElementById('info_tab')) {
                    var footer = document.getElementById('footer');
                    footer.classList.add('footer-tabbed');
                }


                $(window).on('scroll', function() {
                    if ($(window).scrollTop() > 100) {
                        //$('#header').addClass('sticky');
                        $('.mobile-nav-wrap').addClass('sticky');
                    } else {
                        //$('#header').removeClass('sticky');
                        $('.mobile-nav-wrap').removeClass('sticky');
                    }
                });

                $(window).on('scroll', function() {
                    if ($(window).scrollTop() > 100) {
                        $('#header').addClass('sticky');
                        $('.mobile-nav-wrap').addClass('sticky');
                    } else {
                        $('#header').removeClass('sticky');
                        $('.mobile-nav-wrap').removeClass('sticky');
                    }
                });

                $(window).resize(function() {
                    var now_w = $(window).width();

                    if (now_w >= 1920) {
                        $('.bg._left').removeClass('moby');
                    } else if (now_w <= 768) {
                        $('.bg._left').addClass('moby');
                    }
                });

                $(document).ready(function() {
                    var now_w = $(window).width();

                    if (now_w >= 1920) {
                        $('.bg._left').removeClass('moby');
                    } else if (now_w <= 768) {
                        $('.bg._left').addClass('moby');
                    }
                });
            </script>

            <script>
                (function() {
                    var div = document.createElement("div");

                    function guid() {
                        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                            s4() + '-' + s4() + s4() + s4();
                    }

                    function s4() {
                        return Math.floor((1 + Math.random()) * 0x10000)
                            .toString(16)
                            .substring(1);
                    }
                    var user = {
                        id: guid().toUpperCase(),
                        name: 'User-' + Math.floor((1 + Math.random()) * 10000)
                    };
                    var botConnection = new BotChat.DirectLine({
                        token: 'Aop8xYKmqk8.su4a5OOLO0wFccHElRxrQfuKM98f97DbI0JQOPDx_-4',
                        user: user
                    });

                    document.getElementsByTagName('body')[0].appendChild(div);
                    div.outerHTML = "<div id='botDiv' style='width: 600px; height: 0px; margin:10px; position: fixed; bottom: 0; right:0; z-index: 1000;><div  id='botTitleBar' style='height: 40px; width: 600px; position:fixed; '></div></div>";
                    BotChat.App({
                            botConnection: botConnection,
                            user: user,
                            bot: {
                                id: 'botid',
                                name: 'bot name'
                            },
                            resize: 'detect'
                        },
                        document.getElementById("botDiv"));
                    document.getElementsByClassName("wc-header")[0].setAttribute("id", "chatbotheader");
                    document.getElementsByClassName("wc-header")[0].innerHTML = "<span>Bate papo!</span>";
                    document.getElementsByClassName("wc-header")[0].innerHTML += "<button type='button' onclick='doClose()' style='top: 20%; right:2%; position:absolute; color: #FFFFFF; cursor: pointer;border: none'>X</button>";
                    document.getElementById("mychat").addEventListener("click", function(e) {
                        document.getElementById("botDiv").style.height = '70%';
                        document.getElementById("botDiv").style.width = '28%';
                        document.getElementById("botDiv").style.minWidth = "345px";
                        e.target.style.display = "none";

                        botConnection
                            .postActivity({
                                from: user,
                                name: 'setUserIdEvent',
                                type: 'event',
                                value: 'setUserIdEvent'
                            })
                            .subscribe(function(id) {
                                console.log('"trigger setUserIdEvent" sent');
                            });
                    })
                }());
            </script>
            <script>
                function doClose() {
                    var botDiv = document.querySelector('#botDiv');
                    botDiv.style.height = "0px";
                    document.getElementById("mychat").style.display = "inline";
                }
            </script>
            <script type="text/javascript">
                var cor = "";
                var styleElem = document.head.appendChild(document.createElement("style"));
                styleElem.innerHTML = ".wc-message.wc-message-from-bot:before {content: url('https://iterupstorage.blob.core.windows.net/avatar/31.png')}";

                function dataCallback(data) {
                    cor = data.Cor;
                    jQuery('.wc-header').css("background-color", cor);
                }
                jQuery(document).on('DOMNodeInserted', function(e) {
                    jQuery('.wc-card button').css("background-color", cor);
                    jQuery('.wc-app button').css("background-color", cor);
                    jQuery('.wc-message-from-me .wc-message-content').css("background-color", cor);
                    jQuery('.wc-console .wc-mic, .wc-console .wc-send').css("background-color", "#ffffff");
                    jQuery('.wc-card button').mouseover(function() {
                        jQuery(this).css("background-color", "#ffffff");
                        jQuery(this).css("border-color", cor);
                        jQuery(this).css("color", cor);
                    });
                    jQuery('.wc-card button').mouseout(function() {
                        jQuery(this).css("background-color", cor);
                        jQuery(this).css("border-color", "#ffffff");
                        jQuery(this).css("color", "#ffffff");
                    });
                });
            </script>
            <!-- Faz download dacor do txt do azure blob - primeiro passo-->
            <script type="text/javascript" src="https://iterupstorage.blob.core.windows.net/avatar/31.txt"></script>
            <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
            <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
            </body>

            </html>