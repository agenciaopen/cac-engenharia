<?php

$slide_value = tema_lm_get_option('slide-value');
$slide_model = tema_lm_get_option('slide-model');
$time_car_slide = tema_lm_get_option('time-car-slide');
$animate_slide_in = tema_lm_get_option('animate-slide-in');
$animate_slide_out = tema_lm_get_option('animate-slide-out');

?>
<section class="slider<?php if ($slide_model == "boxed"){ echo" boxed"; } ?>">
    <div class="slide-act owl-carousel owl-theme">
        <?php
        if (have_rows('adicionar_slider', $slide_value)) {
            while (have_rows('adicionar_slider', $slide_value)) {
                the_row();
                $imagem = get_sub_field('imagem_em_destaque');
                $titulo = get_sub_field('titulo');
                $texto = get_sub_field('texto');
                $botao = get_sub_field('botao');
                $link = get_sub_field('link');
        ?>
        <div class="item"<?php if ($imagem != ""){ ?> style="background-image:url(<?php echo $imagem; ?>);"<?php } ?>>
            <div class="row align-items-center">
                <div class="col-md-6">
                    <?php if ($titulo != ""){ ?><h2><?php echo $titulo; ?></h2><?php } ?>
                    <?php if ($texto != ""){ echo $texto; } ?>
                    <?php if ($link != ""){ ?><a href="<?php echo $link; ?>"><button><?php if ($botao != ""){ echo $botao; } else { echo "Saiba mais"; } ?></button></a><?php } ?>
                </div>
            </div>
        </div>
        <?php
            }
        }
        ?>
    </div>
</section>
<script type="text/javascript">
jQuery(document).ready(function($) {
$('.slide-act').owlCarousel({
    animateOut: '<?php echo $animate_slide_out; ?>',
    animateIn: '<?php echo $animate_slide_in; ?>',
    items:1,
    loop:true,
    margin:30,
    mouseDrag:false,
    autoplay:true,
    autoplayTimeout:<?php if (tema_lm_get_option('time-car-slide') != '') { echo tema_lm_get_option('time-car-slide'); } else { echo '8000'; } ?>,
    autoplayHoverPause:false,
    smartSpeed:450
});

});
</script>