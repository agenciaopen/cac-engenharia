<?php
add_action( 'init', 'dp_sliders' );
function dp_sliders() {
    $labels = array(
        'name'               => 'Sliders',
        'singular_name'      => 'Slider',
        'menu_name'          => 'Sliders', 
        'name_admin_bar'     => 'Slider',
        'add_new'            => 'Adicionar Slider',
        'add_new_item'       => 'Adicionar novo Slider',
        'new_item'           => 'Novo Slider',
        'edit_item'          => 'Editar Slider',
        'view_item'          => 'Ver Slider',
        'all_items'          => 'Todos Sliders',
        'search_items'       => 'Procurar Sliders',
        'parent_item_colon'  => '',
        'not_found'          => 'Slider não encontrado',
        'not_found_in_trash' => '',
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'slider' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => 8,
        'menu_icon'          => 'dashicons-images-alt2',
        'capability_type'    => 'post',
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'slider', $args );
}
?>