<?php
			global $post;
			$myposts = get_posts( array(
					'posts_per_page' => -1,
					'offset'         => 0,
					'post_type'		 => 'imoveis',
				) 
			);
					
			if ( $myposts ) {
                    setup_postdata( $post ); 
                    $banner_principal = get_field('banner_imovel');
                    $banner_mobile = get_field('banner_imovel_mobile');
                    $banner_secundario = get_field('diferenciais_imagem_lateral');
                    $banner_certificado = get_field('botao_simulacao');
                    $banner_text = get_field('texto_banner_lateral');
                    $post_id = get_the_ID();
                    
?>
       
 
                    <style>
                        .bg_.<?php echo "_identi_".$post_id ?>{
                            background-image: url('<?php echo $banner_principal['url']; ?>');
                        }
                        .bg_.<?php echo "_identi_".$post_id ?>.moby{
                            background-image: url('<?php echo $banner_mobile['url']; ?>');
                        }
                        .bg._float_left.<?php echo "_identi_".$post_id ?>{
                            background-image: url('<?php echo $banner_secundario['url']; ?>');
                        }
                        .bg._certify.<?php echo "_identi_".$post_id ?>{
                            background-image: url('<?php echo $banner_certificado; ?>');
                        }
                    </style>

            <?php if($banner_principal){ ?>
			<div class="bg_ <?php echo "_identi_".$post_id ?>">

                    <div class="_bg_content container">
                        <a href="<?php echo get_field('botao_simulacao_link'); ?>">
                                <div class="bg  _certify <?php echo "_identi_".$post_id ?>"></div>
                        </a>
                    </div>
                    <div class="bg  _float_left <?php echo "_identi_".$post_id ?>"></div>
            </div>
            <?php } else { echo "<style> section.banner-category{display:none;} </style>";} ?>

		<?php
				wp_reset_postdata();
			}
		?>
</div>