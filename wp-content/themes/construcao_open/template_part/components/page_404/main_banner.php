<section class="swiper-container banner">
    <div class="swiper-wrapper banner_controller">    

        <?php
                    global $post;
                    $myposts = get_posts( array(
                            'posts_per_page' => -1,
                            'offset'         => 0,
                            'post_type'		 => 'page',
                            //'post_name' => 'sobre-a-c-a-c'
                        ) 
                    );
                            
                    if ( $myposts ) {
                    foreach ( $myposts as $post ) : 
                            setup_postdata( $post );

                            //print_r($post);
                            $the_id = 188;
                            $banner_principal = get_template_directory_uri() . '/config/img/banner-404.jpg';
                            $banner_secundario = get_field('banner_direita', $the_id);
                            $banner_certificado = get_template_directory_uri() . '/config/src/certificado.svg';
                            $post_id = get_the_ID();
                            
                            if($post_id == 188){
        ?>
        
    
                        <style>
                            .bg._unique.<?php echo "_identi_".$post_id ?>{
                                background-image: url('<?php echo $banner_principal; ?>');
                            }
                            .bg._right.<?php echo "_identi_".$post_id ?>:before{
                                background-image: url('<?php echo $banner_secundario; ?>');
                            }
                            .bg._certify.<?php echo "_identi_".$post_id ?>{
                                background-image: url('<?php echo $banner_certificado; ?>');
                            }
                        </style>
                <div class="swiper-slide bg_ ">        
                    <a href="<?php echo get_field('imagem_certificado_link', 112); ?>">
                                <div class="bg  _unique <?php echo "_identi_".$post_id ?>">
                                            <div class="bg  _certify <?php echo "_identi_".$post_id ?>"></div>
                                </div>
                    </a>
                </div>
            <?php
                } else {}
                endforeach;
                    wp_reset_postdata();
                }
            ?>
    </div>
    
</section>
    
    
    