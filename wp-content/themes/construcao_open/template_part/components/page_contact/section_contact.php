<section class="section-icetab">
    <h3>Escolha um de nossos formulários para entrar em contato:</h3>
    <div id="icetab-container">
        <div class="icetab current-tab">Cliente</div><div class="icetab">Fornecedor</div><div class="icetab">Trabalhe conosco</div><div class="icetab">Venda seu terreno</div>       
    </div>
        
    <div id="icetab-content">
        <div id="call" class="tabcontent tab-active">
            <h3>Esse é um canal direto com a C.A.C.</h3>
            <h4>Preencha os campos abaixo:</h4>
            <?php echo do_shortcode('[contact-form-7 id="120" title="Cliente"]'); ?>
        </div> 
        <div class="tabcontent">
            <h3>Seja um fornecedor também</h3>
            <h4>Preencha os campos abaixo:</h4>
            <?php echo do_shortcode('[contact-form-7 id="358" title="Fornecedor"]'); ?>
        </div>
        <div class="tabcontent">
            <h3>Faça parte da equipe C.A.C Engenharia.</h3>
            <h4>Preencha os campos abaixo:</h4>
            <?php echo do_shortcode('[contact-form-7 id="359" title="Trabalhe conosco"]'); ?>
        </div>
        <div class="tabcontent">
            <h3>Preencha as informações abaixo. <br> Em breve entraremos em contato.</h3>
            <h4>Preencha os campos abaixo:</h4>
            <?php echo do_shortcode('[contact-form-7 id="360" title="Venda seu terreno"]'); ?>
        </div>
    </div> 
</section>


<script language="javascript">
(function ($) {
           const queryString = window.location.search;

            const urlParams = new URLSearchParams(queryString);
            
            const utm_source = urlParams.get('utm_source')
            
            console.log(utm_source);
            if (utm_source == null){
                const utm_source = 'Site';
                console.log(utm_source);

            }
            else{
                const utm_source = urlParams.get('utm_source')
                console.log(utm_source);
            }
    console.log('ok');
    document.addEventListener( 'wpcf7submit', function( event ) {
            let telefoneCompleto = $( "input[name=mask-177]" ).val();
           console.log(telefoneCompleto);
            let tcn = telefoneCompleto.replace('(', '').replace(')', '').replace('-', '').replace(' ', '').replace(' ', '').trim();
           console.log(tcn);
            let ddd = tcn.substring(0,2);
           console.log(ddd);
            let telefone = tcn.substring(2);
           console.log(telefone);
    if ( '120' == event.detail.contactFormId ) {
           console.log( "The contact form ID is 120" );
            // Your code
            let aceite = $( "input[name=acceptance-27]" ).val();
           console.log(aceite);
            let nome = $( "input[name=nome-cliente]" ).val();
           console.log(nome);
            let email = $( "input[name=email-cliente]" ).val();
           console.log(email);
        
            let mensagem = "";
           console.log(mensagem);
            let cpf = "";
           console.log(cpf);
           setTimeout(function() {

            if((aceite !== undefined && aceite !== '') && (nome !== undefined && nome !== '') && (email !== undefined &&  email !=='') && (ddd !== undefined &&  ddd !=='') && (telefone !== undefined &&  telefone !=='')) {
               console.log('preenchidos');
                //chama();
                	let empreendimento = "Nome Empreendimento";
                var leadData = {
                "nome": nome,
                "ddd": ddd,
                "telefone": telefone,
                "email": email,
                "campanha": "CAMPANHA PADRÃO",
                "midia": utm_source,
                "empreendimento": empreendimento
            
                }
                var myJSON = JSON.stringify(leadData);
                console.log(myJSON);
                var settings = {
                "url": "https://api.leads.dommus.com.br/webhook/forms/91232e8508489244c8f8e3f5b626d6cc24784259571b55b0c05966845f26ea62",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "Content-Type": "application/json",
                },
                "data": myJSON,
                };

                $.ajax(settings).done(function (response) {
                console.log(response);
                           setTimeout(function() {

					location = '/obrigado';
                           }, 400);
                });

                    
                    // var ret = hc_envia_mensagem(empreendimento, aceite, nome, email,ddd, telefone,mensagem,cpf);
                    // console.log(ret);
                
                //location = '/obrigado';
            }
            else{ 
               console.log("campos vazios");
            }
           }, 100);
    }
}, false );
})(jQuery);

</script>

