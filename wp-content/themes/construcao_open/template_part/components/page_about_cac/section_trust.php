<section class="bg_section_trust">
    <ul>
        <li>
            <i class="icon_1"></i>
            <p>Há mais de 10 anos</p>
            <p>realizando sonhos!</p>
        </li>
        <li>
            <i class="icon_2"></i>
            <p>4.200</p>
            <p>Unidades em lançamento</p>
        </li>
        <li>
            <i class="icon_3"></i>
            <p>7.000</p>
            <p>Lares entregues</p>
        </li>
        <li>
            <i class="icon_4"></i>
            <p>2.500</p>
            <p>Unidades em andamento</p>
        </li>
    </ul>
</section>