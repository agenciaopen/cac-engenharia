<section class="has-padding full-container advantages-container">
   
    <div class="advantages">
    <h3 class="advantages__h3">Vantagens Competitivas</h3>
        <ul>
            <li class="icon_money">
                <h5>Economia no processo construtivo</h5>
                <p>                
                    Com um processo fabril de larga escala, a C.A.C.
                    otimiza a utilização de materiais e a construção
                    dos empreendimentos, entregando alta qualidade
                    com muita agilidade e economia.
                </p>
            </li>
            <li class="icon_moneyback">
                <h5>Alta produtividade</h5>
                <p>
                    O investimento constante em novas tecnologias
                    e metodologias aumenta a capacidade produtiva
                    e agiliza a entrega dos empreendimentos.
                </p>
            </li>
            <li class="icon_checked">
                <h5>Qualidade total</h5>
                <p>
                    Com foco em entregas diferenciadas, a C.A.C.
                    prioriza a qualidade em todas as etapas da
                    construção de seus empreendimentos,
                    garantindo a satisfação dos clientes.
                </p>
            </li>
            <li class="icon_customer">
                <h5>Foco no cliente</h5>
                <p>  
                    Compromisso com os clientes é um pilar da C.A.C.
                    Por isso, procura respeitar prazos e assegurar
                    que os clientes recebem exatamente o desejado.
                </p>
            </li>
            <li class="icon_rank">
                <h5>Equipe qualificada</h5>
                <p>
                    Os profissionais envolvidos na C.A.C., tanto nos processo
                    gerenciais como construtivos, são altamente qualificados.
                    Além disso, passam por diversos treinamentos  e
                    capacitações, evoluindo constantemente.
                </p>
            </li>
        </ul>
        
    </div>

</section>
<div class="iso"></div>