<section class="section_about">
    <div class="about_imv">
        <div class="two_in">
            <div>
                <h3 class="title_about_imv">Pessoas e sonhos</h3>
                <?php 
                    $the_id = 186;
                    if(get_field('pessoas_e_sonhos')){ ?><p><?php echo get_field('pessoas_e_sonhos'); ?></p>
                <?php } else {} ?>
            </div>
            <div>
                <?php 
                    if(get_field('sobre_a_cac_video') or get_field('sobre_a_cac_imagem')){  $field_video = get_field('sobre_a_cac_video'); $field_img = get_field('sobre_a_cac_imagem'); ?>
                <?php if(get_field('ativar_video') == true) { ?>
                    <div id="thumbnail" class="thumbnail"></div>
                    <!-- <div class='player'>
                        <div id="play-button" class="player">
                            <i class="btn-play" style="background-image:url(<?php echo get_template_directory_uri().'/config/src/play.svg'; ?>)"></i>
                        </div>
                        <div id="pause-button" class="player"></div>
                    </div> -->
                    <div id='media-player'>
                    <div id="ytplayer">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/7NBxuSwF48Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                    <script>
                        var video_caller = function() {
                        var tag = document.createElement('script');
                        tag.src = "https://www.youtube.com/iframe_api";
                        var firstScriptTag = document.getElementsByTagName('script')[0];
                        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                        var get_vdId = document.getElementById('vdID').value;
                        var get_thumbnail = document.getElementById('thumbnail');
                        var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                        var set_victim = document.getElementById('ytplayer');
                        var effect_thumbnail = document.getElementById('thumbnail');
                        var playButton = document.getElementById("play-button");
                        var pauseButton = document.getElementById("pause-button");


                        if (get_vdId.includes('https://youtu.be/') == true) {
                            var str_t = get_vdId.replace('https://youtu.be/', '');

                            get_thumbnail = document.getElementById('thumbnail').innerHTML +=
                                '<img src="https://img.youtube.com/vi/' + str_t + '/maxresdefault.jpg" alt="">';

                            document.getElementById('video').setAttribute('src', 'https://www.youtube.com/embed/' +
                                str_t + '?rel=0?enablejsapi=1&controls=0');

                            playButton.addEventListener('click', function() {
                                pauseButton.classList.toggle("effect_show");
                                playButton.classList.toggle("effect_hide");
                                effect_thumbnail.classList.toggle("thumbnail_effect");
                                document.getElementById('video').src += "&autoplay=1";
                                playButton.style.dysplay = 'none';
                            });

                            pauseButton.addEventListener('click', function() {
                                pauseButton.classList.toggle("effect_show");
                                playButton.classList.toggle("effect_hide");
                                effect_thumbnail.classList.toggle("thumbnail_effect");
                                var deactive = document.getElementById('video').src.replace('&autoplay=1',
                                    '&autoplay=0');
                                document.getElementById('video').setAttribute('src', deactive);
                                playButton.style.dysplay = 'block';
                            });


                        } else {
                            var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');

                            get_thumbnail = document.getElementById('thumbnail').innerHTML +=
                                '<img src="https://img.youtube.com/vi/' + str_t + '/maxresdefault.jpg" alt="">';
                            document.getElementById('video').setAttribute('src', 'https://www.youtube.com/embed/' +
                                str_t + '?rel=0?enablejsapi=1&controls=0');

                            playButton.addEventListener('click', function() {
                                pauseButton.classList.toggle("effect_show");
                                playButton.classList.toggle("effect_hide");
                                effect_thumbnail.classList.toggle("thumbnail_effect");
                                document.getElementById('video').src += "&autoplay=1";
                                playButton.style.dysplay = 'none';
                            });

                                pauseButton.addEventListener('click', function() {
                                    pauseButton.classList.toggle("effect_show");
                                    playButton.classList.toggle("effect_hide");
                                    effect_thumbnail.classList.toggle("thumbnail_effect");
                                    var deactive = document.getElementById('video').src.replace('&autoplay=1',
                                        '&autoplay=0');
                                    document.getElementById('video').setAttribute('src', deactive);
                                    playButton.style.dysplay = 'block';
                                });
                            }
                        }

                        try {
                            video_caller();
                        } catch (error) {
                            console.log(error);
                        }
                    </script>
                <?php }else{?>
                    <img id="if_img" class="if_img" src="<?php echo $field_img; ?>" alt="">
                <?php }} ?>
            </div>
        </div>
    </div>
</section>