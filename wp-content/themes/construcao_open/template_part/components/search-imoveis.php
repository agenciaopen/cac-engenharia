<h3 class="title-search"><?php echo get_field('filtro_texto_titulo', 'options'); ?></h3>

<section class="search">
        <p>Encontre seu imóvel</p>
        <div class="formulario">
            <?= do_shortcode('[searchandfilter slug="imoveis"]'); ?>
        </div>
</section>

<?php get_template_part('/template_part/components/tab_info'); ?>





