<section class="has-container"  id="localizacaoImovel">
    <?php 
        $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
        $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
        $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
        $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
    
    if($fase_da_obra[0] == "Breve Lançamento"){ ?>
    Breve Lançamento
    <?php } else {?>   
        <?php if(get_field('localizacao_empreendimento') or get_field('localizacao_estande_de_vendas')){ ?> 
        <div class="map_controller">
        <h3>Excelente Localização</h3>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/imask/5.2.1/imask.min.js" integrity="sha256-xj2lSOVYUEOObcK2OI0cA7XiLDn0TgBzWeVcQuLcMtw=" crossorigin="anonymous"></script>

        <?php 
        //https://www.google.com.br/maps/dir/30512-580/R.+Dalva+Raposo+-+Maria+Paula,+São+Gonçalo+-+RJ/
        $location = get_field('localizacao_empreendimento');
        $location2 = get_field('localizacao_estande_de_vendas');
        ?>
        <div class="address">
        <?php
            $address = '';
            foreach( array('street_number', 'street_name', 'city', 'state', 'post_code', 'country') as $i => $k ) {
                if( isset( $location[ $k ] ) ) {
                    $address .= sprintf( '<span class="segment-%s">%s</span>, ', $k, $location[ $k ] );
                }
            }
            $address = trim( $address, ', ' );
            echo '<p>' . $address . '.</p>';

            if($location2){
                $address2 = '';
                foreach( array('street_number', 'street_name', 'city', 'state', 'post_code', 'country') as $i => $k ) {
                    if( isset( $location2[ $k ] ) ) {
                        $address2 .= sprintf( '<span class="segment-%s">%s</span>, ', $k, $location2[ $k ] );
                    }
                }
                $address2 = trim( $address2, ', ' );
                echo '<p>' . $address2 . '.</p>';
            }

        ?>
        </div>
            <div class="pos_maps_routes">
                <?php if($location) {?>
                    <div class="map_empreendimento">
                        <h3>Como chegar</h3>
                        
                        <span id="err_em"></span>
                        <input id="CEP_empreendimento" type="text" placeholder="Digite o seu CEP">
                        <input id="empreendimento_rota" type="hidden" value="<?php echo $location['post_code']; ?>">
                        <button id="calcular_rota">Ver rota ></button>
                    </div>
                    <style>

                            input#CEP_empreendimento{
                                opacity: .8;
                                font-size: 12px;
                                text-decoration: underline;
                               
                            }
                            

                    </style>
                    <script>
                        var getroute_empreendimento = function(){
                                    var call_route = document.getElementById('calcular_rota');
                                    var get_route_e = document.getElementById('CEP_empreendimento');

                                    get_route_e.addEventListener('change', function(){
                                        console.log(get_route_e.value);
                                    });
                                    
                                    var err_em = document.getElementById('err_em');
                                    err_em.innerHTML += '';
                                    
                                    call_route.addEventListener('click', function(){
                                        var get_route_go_e = document.getElementById('empreendimento_rota').value;
                                        var str_route = "https://www.google.com.br/maps/dir/"+get_route_e.value+"/"+get_route_go_e+"/";
                                        if(get_route_e.value == ''){
                                            var err_em = document.getElementById('err_em');
                                            err_em.classList.toggle('active');
                                            if(err_em.classList.contains('active') == true){
                                            err_em.innerText = "O cálculo da rota só pode ser feito com o CEP";
                                            } else if(err_em.classList.contains('active') == false){
                                            err_em.innerText = "";
                                            }
                                        }else{
                                            window.open(str_route, "_blank");
                                            var err_em = document.getElementById('err_em');
                                            err_em.classList.toggle('active');
                                            err_em.innerText = "";
                                        }
                                    });
                                    }

                        var maskOptions = {
                            mask: '00000-000'
                        };
                        var mask_em = IMask(CEP_empreendimento, maskOptions);
                        getroute_empreendimento();
                    </script>
                <?php  } ?>
                <?php if($location2) { ?>
                        <div class="map_estande">
                            <h3>Como chegar</h3>
                            <h5>Estande de vendas</h5>
                            <span id="err_es"></span>
                            <input id="CEP_estande" onchange="getroute_estande()" type="text">
                            <input id="estande_rota" type="hidden" value="<?php echo $location2['post_code']; ?>">
                            <button id="calcular_estande_rota">Ver rota ></button>
                        </div>
                        <script>
                            var maskOptions = {
                                mask: '00000-000'
                            };
                            var mask_es = IMask(CEP_estande, maskOptions);
                            getroute_estande();  
                        </script>
                <?php } ?>
        
        </div>


        <?php

        if( $location ): ?>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAY4qjaTkMG70PlTq2fsx2OuXtKyyZiz_c"></script>

            <script>
            var getroute_estande = function(){
            var call_routes = document.getElementById('calcular_estande_rota');
            var get_route_es = document.getElementById('CEP_estande').value;

            call_routes.addEventListener('click', function(){
                var get_route_go_es = document.getElementById('estande_rota').value;
                var str_route = "https://www.google.com.br/maps/dir/"+get_route_es+"/"+get_route_go_es+"/";
                if(get_route_es == ''){
                var err_es = document.getElementById('err_es');
                err_es.classList.toggle('active');
                if(err_es.classList.contains('active') == true){
                    err_es.innerText = "O cálculo da rota só pode ser feito com o CEP";
                } else if(err_es.classList.contains('active') == false){
                    err_es.innerText = "";
                }
                }else{
                window.open(str_route, "_blank");
                var err_es = document.getElementById('err_es');
                err_es.classList.toggle('active');
                err_es.innerText = "";
                }
            });

            }

            var CEP_empreendimento = document.getElementById('CEP_empreendimento');
            var CEP_estande = document.getElementById('CEP_estande');
            var maskOptions = {
            mask: '00000-000'
            };

            var mask_em = IMask(CEP_empreendimento, maskOptions);
            var mask_es = IMask(CEP_estande, maskOptions);
            </script>

            <div class="acf-map" data-zoom="16">
            <?php if($location) { ?> <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div> <?php } ?>
            <?php if($location2) { ?><div class="marker" data-lat="<?php echo esc_attr($location2['lat']); ?>" data-lng="<?php echo esc_attr($location2['lng']); ?>"></div> <?php } ?>
            </div>

        <?php endif; ?>

    </div>

    <?php } ?>
        
    <?php } ?>
</section>