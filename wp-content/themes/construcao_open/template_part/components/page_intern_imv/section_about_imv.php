<section class="has-container"  id="sobreImovel">

    <?php 
        $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
        $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
        $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
        $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
    
    if($fase_da_obra[0] == "Breve Lançamento"){ ?>
            <div class="breve_lancamento_title">
                <h3 class="title_about_imv">Sobre o imóvel</h3>
                <p><?php echo get_field('sobre_o_imovel');?></p>
            </div>
    <?php } else {?>
    <div class="about_imv">
        <div class="two_in">
            <?php if(get_field('sobre_o_imovel')){ ?>
            <div>
                <h3 class="title_about_imv">Sobre o imóvel</h3>
                <p><?php echo get_field('sobre_o_imovel');?></p>
            </div>
            <?php } else {} ?>
            <?php if(get_field('sobre_o_imovel_video') or get_field('sobre_o_imovel_imagem')){  $field_video = get_field('sobre_o_imovel_video'); $field_img = get_field('sobre_o_imovel_imagem'); ?>
            <?php if(get_field('ativar_video') == true){?>
            <?php if($field_video) { ?>
            <div>
                <div id="thumbnail" class="thumbnail"></div>
                
                <div id='media-player'>
                
                    <div id="ytplayer">
                        <iframe id="video" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="auto" height="auto" type="text/html" src=""></iframe>
                    </div>
                    <input id="vdID" type="hidden" value="<?php echo get_field('sobre_o_imovel_video'); ?>">
                </div>
                    <script>
                        var video_caller = function() {
                        var get_vdId = document.getElementById('vdID').value;
                        var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                        var set_victim = document.getElementById('ytplayer');

                        if (get_vdId.includes('https://youtu.be/') == true) {
                            var str_t = get_vdId.replace('https://youtu.be/', '');

                            document.getElementById('video').setAttribute('src', 'https://www.youtube.com/embed/' +
                                str_t + '?rel=0?enablejsapi=1&autoplay=0');

                        } else {
                            var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                            document.getElementById('video').setAttribute('src', 'https://www.youtube.com/embed/' +
                                str_t + '?rel=0?enablejsapi=1&autoplay=0');
                            }
                        }

                        try {
                            video_caller();
                        } catch (error) {
                            console.log(error);
                        }
                    </script>
            </div>
            <?php }else{}?>
            <?php } else {?>
            <?php if($field_img) { ?>
                <div>
                    <img class="if_img" src="<?php echo $field_img; ?>" alt="">
                </div>
            <?php }else{}?>
            <?php } ?>
            <?php } else {} ?>
        </div>
    </div>
    <?php } ?>
</section>