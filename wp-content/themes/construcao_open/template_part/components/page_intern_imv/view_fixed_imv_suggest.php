<section class="has-container">

<h3>
    Veja outros imóveis semelhantes a esse!
</h3>

<?php 

$imv_posts = get_posts( array(
    'posts_per_page' => -1,
    'offset'         => 0,
    'post_type'		 => 'imoveis',
    ) 
);
        
if ( $imv_posts ) { ?>

<section class="swiper-container results">
        <div class="swiper-wrapper content-loop">
                <?php
                        foreach ( $imv_posts as $post ) : 
                            setup_postdata( $post ); 
                            $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
                            $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
                            $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
                            $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
                            $faixa_de_preco = wp_get_post_terms($post->ID, 'faixa_de_preco', array("fields" => "names"));
                            $icons = get_field('icones_blocos', 'option');
                ?>          
        
                <div id="loop-emp" class="swiper-slide results-list">
                    <div class=" search-filter-result-item post">
                    <a href="<?php the_permalink(); ?>">
                            <div><?= the_post_thumbnail(); ?></div>
                            <?php if($cidade[0] && $estado[0]) {?>
                            <div class="result_content_controller">
                                <?php if(get_field('residencial')){ ?><p><?php echo get_field('residencial') ?></p><?php }else{} ?>
                                <?php if($cidade[0] && $estado[0]) { ?><h3><?= $cidade[0]; ?><span><?= $estado[0]; ?></span></h3> <?php } else {} ?>
                                <?php if(get_field('condominio')) { ?><p> Condomínio <?php echo get_field('condominio') ?></p><?php } else {} ?>
                                <?php
                                    if($quarto[0]){
                                    while( have_rows('icones_blocos', 'options') ): the_row(); 
                                ?>
                                <style>
                                    .icon.icon-moby{
                                        background-image:url(<?php echo get_sub_field('quartos_icon')['url'];?>);
                                    }
                                    .icon.icon-req{
                                        background-image:url(<?php echo get_sub_field('medidas_icon')['url'];?>);
                                    }
                                </style>
                                <?php
                                    endwhile;
                                }else{}
                                ?>

                                <?php if($quarto[0]){?><p><i class="icon icon-moby"></i><?= $quarto[0]; ?> </p> <?php } else {} ?>

                                <?php if(get_field('medidas') || get_field('medidas_2')){ ?>
                                    <div class="medidas">
                                        <p class="m2"><i class="icon icon-req"></i><?php echo get_field('medidas') ?> m</p>
                                        <?php if(get_field('medidas_2')){?><span class="m2 left">a <?php echo get_field('medidas_2') ?> m</span><?php }else{}?> 
                                    </div>
                                <?php } else {}?>
                            </div>
                                <?php } else { echo "preencha, cidade e estado."; } ?>
                        </a>
                    </div>
                </div>

        <?php 
            endforeach;
            wp_reset_postdata();

        ?>
    </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
        <div class="-swiper-pagination"></div>
</section> 


<section class="btn_center">
<a href="<?php echo get_site_url(); ?>/imoveis">
    <button class="btn_red">Ver mais imóveis</button>
</a>
</section>

<?php
}
?>
</section>