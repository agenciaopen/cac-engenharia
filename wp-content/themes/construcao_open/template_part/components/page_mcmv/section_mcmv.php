<section class="">
    <?php if (get_field('banner_desktop')) : ?>
        <img src="<?php the_field('banner_desktop'); ?>" loading="lazy" srcset="" class="contato__banner--desktop" />
    <?php endif ?>
    <?php if (get_field('banner_mobile_mcmv')) : ?>
        <img src="<?php the_field('banner_mobile_mcmv'); ?>" class="contato__banner--mob" />
    <?php endif ?>

</section>



<section class="has-no-padding-left mb cva-apresentacao">
    <p class="cva__texto">
        <?php echo get_field('cva_texto', false, false); ?>


    </p>
    <?php $cva_botao = get_field('cva_botao'); ?>
    <?php if ($cva_botao) : ?>
        <a href="https://www.cacengenharia.com.br/contato/" class="cva__botao">Quero falar com um consultor</a>
    <?php endif; ?>
</section>

<section class="has-no-padding-left cva__inscricao ">

    <h3 class="cva__inscricao__titulo">
        <?php the_field('cva_inscricao_titulo', false, false); ?>
    </h3>
    <div class="cva__inscricao__itens">
        <?php if (have_rows('cva_inscricao_icones')) : ?>
            <?php while (have_rows('cva_inscricao_icones')) : the_row(); ?>


                <div class="itens">
                    <?php if (get_sub_field('repetidor_icone')) : ?>
                        <div class="img-seta-desk">
                            <img src="<?php the_sub_field('repetidor_icone'); ?>" />
                            <!-- <svg width="50" height="26" viewBox="0 0 50 26" fill="none" xmlns="http://www.w3.org/2000/svg" class="seta--desk">
                                <path d="M49.4271 11.6175C49.4266 11.6169 49.4261 11.6162 49.4254 11.6156L39.2199 1.45937C38.4554 0.698529 37.2188 0.70136 36.4577 1.46601C35.6968 2.23056 35.6997 3.46718 36.4643 4.22812L43.3162 11.0469H1.95312C0.874414 11.0469 0 11.9213 0 13C0 14.0787 0.874414 14.9531 1.95312 14.9531H43.3161L36.4644 21.7719C35.6998 22.5328 35.6969 23.7694 36.4578 24.534C37.2188 25.2987 38.4556 25.3014 39.22 24.5406L49.4255 14.3844C49.4261 14.3838 49.4266 14.3831 49.4272 14.3825C50.1922 13.619 50.1897 12.3784 49.4271 11.6175Z" fill="#D60812" />
                            </svg> -->
                        </div>
                    <?php endif ?>

                    <h4 class="cva__inscricao__subtitulo"><?php the_sub_field('repetidor_titulo'); ?></h4>
                    <p class="cva__inscricao__texto"><?php the_sub_field('repetidor_texto', false, false); ?></p>
                    <svg width="25" height="50" viewBox="0 0 25 50" fill="none" xmlns="http://www.w3.org/2000/svg" class="seta--mob">
                        <path d="M14.2731 49.4271C14.2737 49.4266 14.2744 49.4261 14.275 49.4254L24.4313 39.2199C25.1921 38.4554 25.1893 37.2188 24.4246 36.4577C23.6601 35.6968 22.4234 35.6997 21.6625 36.4643L14.8438 43.3162L14.8438 1.95312C14.8438 0.874414 13.9693 -4.82165e-07 12.8906 -5.29317e-07C11.8119 -5.76469e-07 10.9375 0.874413 10.9375 1.95312L10.9375 43.3161L4.11875 36.4644C3.35782 35.6998 2.1212 35.6969 1.35664 36.4578C0.591898 37.2188 0.589262 38.4556 1.35 39.22L11.5063 49.4255C11.5068 49.4261 11.5075 49.4266 11.5081 49.4272C12.2716 50.1922 13.5122 50.1897 14.2731 49.4271Z" fill="#D60812" />
                    </svg>


                </div>
            <?php endwhile; ?>
        <?php else : ?>

        <?php endif; ?>
    </div>
</section>

<section class="has-no-padding-left">

    <h3 class="cva__saber__titulo">
        <?php the_field('cva_quero_saber_mias_texto', false, false); ?>


    </h3>
    <?php $cva_botao = get_field('cva_botao'); ?>
    <?php if ($cva_botao) : ?>
        <a href="https://www.cacengenharia.com.br/contato/" class="cva__botao">Quero falar com um consultor</a>
    <?php endif; ?>

</section>

<section class="has-no-padding-left duvidas">
    <h3 class="cva__duvida__titulo">
        <?php the_field('cva_duvidas_frequentes_titulo', false, false); ?>
    </h3>
    <?php if (have_rows('cva_duvidas_frequentes_repetidor')) : ?>
        <?php while (have_rows('cva_duvidas_frequentes_repetidor')) : the_row(); ?>
            <h4 class="cva__inscricao__subtitulo"><?php the_sub_field('titulo'); ?></h4>
            <p class="cva__inscricao__texto"><?php the_sub_field('texto', false, false); ?></p>
            <hr class="hr-duvida">
        <?php endwhile; ?>
    <?php else : ?>

    <?php endif; ?>


</section>