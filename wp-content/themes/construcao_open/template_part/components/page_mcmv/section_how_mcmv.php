<section class="has-no-padding-left bottom bg_gray bg_il">
    <div class="mcmv">
        <h3>Como Funciona o Minha Casa Minha Vida</h3>
        <div class="timeline_controller">
            <ul class="timeline_how">
                <li class="bubble_dash d1">
                    <h4>Faça seu cadastro</h4>
                    <p>Se você tem uma renda familiar de até
                        R$1.300, pode participar do MCMV. 
                    </p>
                    <p>Para inscrever-se, só precisa conversar diretamente com seu corretor. </p>
                </li>
                <li class="bubble_dash d2">
                    <h4>É hora de escolher seu apartamento</h4>
                    <p>Enquanto você aguarda o contato do banco, é hora de escolher empreendimento da C.A.C. vai ser sua nova casa. </p>
                </li>
                <li class="bubble_dash d3">
                    <h4>Receba a avaliação do Cadastro</h4>
                    <p>Você só precisa esperar o banco analisar seus documentos.</p>
                    <p>Se forem aprovados, decida com o banco qual é o melhor financiamento para você.</p>
                </li>
                <li class="bubble_dash d4">
                    <h4>Assine o seu Contrato</h4>
                    <p>Depois de encontrar o apartamento perfeito, é hora de fechar negócio e assinar seu contrato. </p>
                </li>
            </ul>
        </div>
    </div>
</section>