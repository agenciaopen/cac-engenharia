<section class="info_mcmv">
<ul>
        <li>
            <h3>Posso usar meu FGTS no Minha Casa Minha Vida?</h3>
            <p>Pode sim! Você só precisa ficar de olho em algumas condições:</p>
            <ul>
                <li><p>Não estar comprando imóvel residencial pelo SHF.</p></li>
                <li><p>Não ter nenhum imóvel residêncial no município onde você mora ou trabalha. E também nos municípios vizinhos.</p></li>
            </ul>
        </li><li>
            <h3>Vou saber o valor exato do financiamento?</h3>
            <p>Com certeza! Além de ser um direito seu, é muito importante que o banco ou a construtora forneçam todos os valores do seu financiamento.</p>
        </li><li>
            <h3>Quais documentos eu preciso para me inscrever?</h3>
            <ul>
                <li><p>Documento oficial de identidade</p></li>
                <li><p>CPF</p></li>
                <li><p>Comprovante do estado civil</p></li>
                <li><p>Comprovante de residência</p></li>
                <li><p>Comprovação de renda</p></li>
                <li><p>Declaração pessoal de saúde, se o financiamento for pelo SBPE</p></li>
                <li><p>Declaração do IR ou Declaração de Isento</p></li>
                <li><p>Documentação do vendedor</p></li>
                <li><p>Documentação do imóvel</p></li>
            </ul>
        </li>
        
        
        <li>
            <h3>Quais as vantagens?</h3>
            <ul>
                <li><p>Mais facilidade no financiamento</p></li>
                <li><p>Taxa de juros mais baixas</p></li>
                <li><p>Prazos maiores</p></li>
                <li><p>Ajuda do subsídio do governo</p></li>
                <li><p>Parcelas decrescente</p></li>
            </ul>
        </li>
    </ul>
</section>