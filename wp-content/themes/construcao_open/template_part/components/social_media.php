
    <?php $redes_sociais = get_field('redes_sociais', 'option'); ?>

    <?php if( have_rows('redes_sociais', 'options') ): ?>
        <?php while( have_rows('redes_sociais', 'options') ): the_row(); 
            $facebook = get_sub_field('facebook');
            $instagram = get_sub_field('instagram');
            $twitter = get_sub_field('twitter');
            $linkedin = get_sub_field('linkedin');
            $youtube = get_sub_field('youtube');
            ?>
            <ul class="social_media social-ajustes">
            <li class="linkedin">
                        <a href="https://www.linkedin.com/company/cac-engenharia" target="_blank"><i class="icon icon-linkedin"></i></a>
                    </li>
                <?php if($facebook != '') { ?> 
                    <li class="facebook">
                        <a href="<?php echo esc_url( $facebook ); ?>" target="_blank" ><i class="icon icon-facebook" ></i></a>
                    </li>
                <?php } else { } ?>
                <?php if($instagram != '') { ?> 
                    <li class="instagram">
                        <a href="<?php echo esc_url( $instagram ); ?>" target="_blank"><i class="icon icon-instagram"></i></a>
                    </li>
                <?php } else { } ?>
                <?php if($twitter != '') { ?> 
                    <li class="twitter">
                        <a href="<?php echo esc_url( $twitter ); ?>" target="_blank"><i class="icon icon-twitter"></i></a>
                    </li>
                <?php } else { } ?>
                
                  
               
                <?php if($youtube != '') { ?> 
                    <li class="youtube">
                        <a href="<?php echo esc_url( $youtube ); ?>" target="_blank"><i class="icon icon-youtube-play"></i></a>
                    </li>
                <?php } else { } ?>
            </ul>
        <?php endwhile; ?>
    <?php endif; ?>
