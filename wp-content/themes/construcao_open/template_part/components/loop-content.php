<?php

$fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
$estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
$cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
$quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
$faixa_de_preco = wp_get_post_terms($post->ID, 'faixa_de_preco', array("fields" => "names"));
$icons = get_field('icones_blocos', 'option');
if($fase_da_obra[0] == "Lançamento"){$fase_da_obra[0] = "Lancamento";}else if($fase_da_obra[0] == "Breve Lançamento") { $fase_da_obra[0] = "Breve_Lancamento";} else if($fase_da_obra[0] == "Documentação Grátis") { $fase_da_obra[0] = "Documentacao_gratis";} else {}; 
if($quarto[0] == "01"){ $quarto[0] = "01 Quarto";}else if($quarto[0] == "02"){ $quarto[0] = "02 Quartos";} else if ($quarto[0] == "03"){ $quarto[0] = "03 Quartos";} else if ($quarto[0] == "04"){ $quarto[0] = "04 Quartos";} else if ($quarto[0] == "05"){ $quarto[0] = "05 Quartos";}else if ($quarto[0] == "06"){ $quarto[0] = "06 Quartos";};
$theid = get_the_ID();
$counter = 0;
$active_cond = get_field('iconetexto_condominio', $postid);
$active_laz  = get_field('iconetexto_lazer', $postid);

?>

<div id="loop-emp" class="swiper-slide results-list">
                <div class=" search-filter-result-item post">
                    <a href="<?php the_permalink(); ?>">
                        <div class="image_results_controller _<?php echo $theid; ?>"></div>
                        <?php if($cidade[0] && $estado[0]) {?>
                        <div class="result_content_controller">
                            <?php if(get_field('residencial')){ ?><p><?php echo get_field('residencial') ?></p>
                            <?php }else{} ?>
                            <?php if($cidade[0] && $estado[0]) { ?><h3><?= $cidade[0]; ?><span><?= $estado[0]; ?></span>
                            </h3> <?php } else {} ?>
                            <?php if(get_field('condominio')) { ?><p> Condomínio <?php echo get_field('condominio') ?>
                            </p><?php } else {} ?>
                            <?php if($fase_da_obra[0] or get_field('itbi_info')){?>
                            <div class="tags">
                                <?php if($fase_da_obra[0]) { ?><p class="tag on <?= $fase_da_obra[0] ?>">
                                    <?php if($fase_da_obra[0] == "Lancamento"){echo $fase_da_obra[0] = "Lançamento";} else if($fase_da_obra[0] == "Breve_Lancamento"){echo $fase_da_obra[0] = "Breve Lançamento";} else if($fase_da_obra[0] == "Documentacao_gratis"){echo $fase_da_obra[0] = "Documentação Grátis";} else { echo $fase_da_obra[0]; }; ?>
                                </p> <?php } else {} ?>
                                <?php if(get_field('itbi_info')) { ?><p
                                    class="tag off <?php if( get_field('itbi_info') == "Documentação Grátis" ) { echo "Documentacao_gratis"; } else {  echo get_field('itbi_info');  } ?>">
                                    <?php echo get_field('itbi_info'); ?></p> <?php } else {} ?>
                            </div>
                            <?php }else{} ?>

                            <?php
                                if($quarto[0]){
                                while( have_rows('icones_blocos', 'options') ): the_row(); 
                            ?>
                            <style>
                            .icon.icon-moby {
                                background-image: url(<?php echo get_sub_field('quartos_icon')['url'];
                                ?>);
                            }

                            .icon.icon-req {
                                background-image: url(<?php echo get_sub_field('medidas_icon')['url'];
                                ?>);
                            }

                            .icon.icon-con {
                                background-image: url(<?php echo get_sub_field('comdominio_icon')['url'];
                                ?>);
                            }

                            .icon.icon-laz {
                                background-image: url(<?php echo get_sub_field('lazer_icon')['url'];
                                ?>);
                            }
                            </style>
                            <?php
                                endwhile;
                            }else{}
                            ?>

                            <?php if($quarto[0]){?><p><i class="icon icon-moby"></i><?= $quarto[0]; ?> </p>
                            <?php } else {} ?>

                            <?php if(get_field('medidas') || get_field('medidas_2')){ ?>
                            <div class="medidas">
                                <p class="m2"><i class="icon icon-req"></i><?php echo get_field('medidas') ?> m</p>
                                <?php if(get_field('medidas_2')){?><span class="m2 left">a
                                    <?php echo get_field('medidas_2') ?> m</span><?php }else{}?>
                            </div>



                            <?php if ($active_cond == true) { ?>
                                <div class="condominio">
                                    <p><i class="icon icon-con"></i><?php echo get_field('condominio') ?></p>
                                </div>
                            <?php } else { }?>

                            <?php if ($active_laz == true) { ?>
                                <div class="lazer">
                                    <p><i class="icon icon-laz"></i><?php echo get_field('texto_lazer') ?></p>
                                </div>
                            <?php } else { }?>




                            <?php } else {}?>
                        </div>
                        <?php } else { echo "preencha, cidade e estado."; } ?>
                    </a>
                </div>
            </div>