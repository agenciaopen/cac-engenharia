<?php
    
    $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
    $banner_imovel = get_field('banner_interno');
    $banner_certificado = get_field('banner_interno_certificado');
    $logo_imovel = get_field('imagem_logo_imovel');
    $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
    $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
    $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
    $faixa_de_preco = wp_get_post_terms($post->ID, 'faixa_de_preco', array("fields" => "names"));
    $icons = get_field('icones_blocos', 'option');

?>     