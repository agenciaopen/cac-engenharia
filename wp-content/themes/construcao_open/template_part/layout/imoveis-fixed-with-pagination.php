
        <?php
        //search imoveis
        get_template_part('template_part/components/search-imoveis');
        //fixed imoveis
        //get_template_part('template_part/layout/imoveis-fixed-result');
        ?>

<style>
    <?php $imv_posts = get_posts(array(
        'post_type' => 'imoveis',
        'posts_per_page' => -1,
    ));


    if ($imv_posts) {
        foreach ($imv_posts as $post) : setup_postdata($post);
            $theid = get_the_ID();
    ?>.image_results_controller._<?php echo $theid;

                                    ?> {
        background-image: url(<?= the_post_thumbnail_url();
                                ?>);
    }

    <?php endforeach;
    }

    ?>
</style>

<?php

$args = array(
    'post_type' => 'imoveis',
    'posts_per_page' =>  -1,
    'orderby'   => 'meta_value',
    'order' => 'ASC',
);

$loop = new WP_Query($args);
if ($loop->have_posts()) { ?>
    <section id="if_moby">
        <section class="swiper-container results">
            <div class="swiper-wrapper content-loop">
                <?php
                while ($loop->have_posts()) : $loop->the_post();

                    $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));

                    get_template_part('template_part/components/loop-content');

                endwhile;
                ?>
            </div>
        </section>
        <div class="swiper-button-prev pagination-position-arrow"></div>
        <div class="swiper-button-next pagination-position-arrow"></div>
        <div class="swiper-pagination pagination-position"></div>
    </section>






<?php
}
wp_reset_postdata();
?>