    <?php 
        if( have_rows('secao_confianca_imagens', 'options') ):
        while( have_rows('secao_confianca_imagens', 'options') ): the_row(); 
    ?>
        <style>
            .section_trust.bg_{
                background-image:url(<?php echo get_sub_field('secao_confianca_background'); ?>);
            }

            .icon.icon_cert_1{
                background-image:url(<?php echo get_sub_field('secao_icone_1'); ?>);
            }
            .icon.icon_cert_2{
                background-image:url(<?php echo get_sub_field('secao_icone_2'); ?>);
            }
            .icon.icon_cert_3{
                background-image:url(<?php echo get_sub_field('secao_icone_3'); ?>);
            }

            <?php 
                if( have_rows('secao_confianca_imagens_bloco', 'options') ):
                while( have_rows('secao_confianca_imagens_bloco', 'options') ): the_row(); 
            ?>
                .info_presentation_1.bg_{
                    background-image:url(<?php echo get_sub_field('secao_confianca_background_1'); ?>);
                }
                .info_presentation_2.bg_{
                    background-image:url(<?php echo get_sub_field('secao_confianca_background_2'); ?>);
                }
                .info_presentation_3.bg_{
                    background-image:url(<?php echo get_sub_field('secao_confianca_background_3'); ?>);
                }

                .icon.icon_1{
                    background-image:url(<?php echo get_sub_field('secao_icone_1'); ?>);
                }
                .icon.icon_2{
                    background-image:url(<?php echo get_sub_field('secao_icone_2'); ?>);
                }
                .icon.icon_3{
                    background-image:url(<?php echo get_sub_field('secao_icone_3'); ?>);
                }
            <?php 
                endwhile; 
                endif; 
            ?>

        </style>
        <section class="section_trust bg_">
            <div class="trust_space">
                <div class="two_in">
                    <div class="trust_controller">
                        <div class="title_trust_call">
                            <h3>
                                <?php echo get_field('secao_confianca_titulo', 'options'); ?>
                            </h3>
                            <p>
                            <?php echo get_field('secao_confianca_subtitulo', 'options'); ?>
                            </p>

                            <div class="trust_certify_pos hide-desktop">
                                <i class="icon icon_cert_1"></i>
                                <i class="icon icon_cert_2"></i>
                                <i class="icon icon_cert_3"></i>
                            </div>

                            <a href="<?php echo site_url()."\/sobre-a-c-a-c"; ?>" class="btn_red">Conheça a C.A.C Engenharia</a>
                        </div>
                        <div class="info_presentation_1 bg_">
                            <i class="icon icon_1"></i>
                                <?php 
                                    if( have_rows('secao_confianca_blocos', 'options') ):
                                    while( have_rows('secao_confianca_blocos', 'options') ): the_row(); 
                                ?>
                                        <ul>
                                            <li><?php echo get_sub_field('bloco_texto_1'); ?></li>
                                            <li><?php echo get_sub_field('bloco_texto__sub_1'); ?></li>
                                        </ul>
                                <?php 
                                    endwhile; 
                                    endif; 
                                ?>
                        </div>
                        <div class="info_presentation_2 bg_">
                            <i class="icon icon_2"></i>
                            <?php 
                                    if( have_rows('secao_confianca_blocos', 'options') ):
                                    while( have_rows('secao_confianca_blocos', 'options') ): the_row(); 
                                ?>
                                        <ul>
                                            <li><?php echo get_sub_field('bloco_texto_2'); ?></li>
                                            <li><?php echo get_sub_field('bloco_texto__sub_2'); ?></li>
                                        </ul>
                                <?php 
                                    endwhile; 
                                    endif; 
                                ?>
                        </div>
                        <div class="info_presentation_3 bg_">
                            <i class="icon icon_3"></i>
                            <?php 
                                    if( have_rows('secao_confianca_blocos', 'options') ):
                                    while( have_rows('secao_confianca_blocos', 'options') ): the_row(); 
                                ?>
                                        <ul>
                                            <li><?php echo get_sub_field('bloco_texto_3'); ?></li>
                                            <li><?php echo get_sub_field('bloco_texto__sub_3'); ?></li>
                                        </ul>
                                <?php 
                                    endwhile; 
                                    endif; 
                                ?>
                        </div>
                    </div>
                </div>
        
                <div class="trust_certify_pos hide-mobile">
                    <i class="icon icon_cert_1"></i>
                    <i class="icon icon_cert_2"></i>
                    <i class="icon icon_cert_3"></i>
                </div>
            </div>
        </section>
    <?php 
        endwhile; 
        endif; 
    ?>