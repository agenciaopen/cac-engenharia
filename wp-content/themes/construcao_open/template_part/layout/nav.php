<nav id="header" class="hide-mobile">
    <?php 
    $logo_principal = get_field('logo_principal', 'option');
    $favicon = get_field('favicon', 'option');
    ?>    
    <div class="flex-in-nav">
            <div class="logo">
                <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name');?>" alt="<?php bloginfo('name');?>">
                    <img src="<?php if ($logo_principal != '') {echo $logo_principal;} else {echo get_template_directory_uri() . '/build/imgs/demo/logo.png';}?>" alt="<?php bloginfo('name');?>" class="logo-ajuste" />
                </a>
            </div>
            
            <!--botao busca-->
            <div class="search-form">
            <!-- <i class="icon icon-lupa"></i> --><?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
            </div>
            
        
        <?php
            $defaults = array(
                'theme_location' => 'principal',
                'container' => 'div',
                'container_class' => 'menu-principal',
                'echo' => true,
                'fallback_cb' => 'wp_page_menu',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth' => 0,
            );
            wp_nav_menu($defaults);
        ?>
    </div>
    <?php
        //redes sociais
        //get_template_part('template_part/components/social_media');
    ?>
</nav>