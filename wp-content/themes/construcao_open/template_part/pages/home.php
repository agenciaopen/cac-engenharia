<?php
	//search imoveis
	get_template_part('template_part/components/search-imoveis');
	//fixed imoveis
	//get_template_part('template_part/layout/imoveis-fixed-result');
?>
    <section class="swiper-container launch">
        <?php get_template_part('/template_part/components/show_launch'); ?>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
	</section>
	<div id="container">
        <img id="mychat" src="<?php echo get_field('chatbot', 'options'); ?>"/>		
</div>
<?php
	//section cta
	get_template_part('template_part/layout/section_cta');
	//section cta_trust
	get_template_part('template_part/layout/section_trust');
?>