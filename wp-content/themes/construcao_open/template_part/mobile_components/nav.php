<?php
$logo_principal = get_field('logo_principal_mobile', 'option');
?>

<nav class="mobile-nav-wrap hide-desktop" role="navigation">
    <div class="logo">
        <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>" alt="<?php bloginfo('name'); ?>">
            <img src="<?php if ($logo_principal != '') {
                            echo $logo_principal;
                        } else {
                            echo get_template_directory_uri() . '/build/imgs/demo/logo.png';
                        } ?>" alt="<?php bloginfo('name'); ?>" />
        </a>
    </div>
    <div class="search-form">
        <!-- <i class="icon icon-lupa"></i> --><?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
    </div>
    <?php //get_template_part('template_part/components/simple-search'); 
    ?>
    <div class="js-menu menu-toggle">
        <div class="hamburger-menu" id="burger"></div>
    </div>
</nav>

<menu class="menu hide-desktop">
    <nav class="nav-menu-desktop">
        <div class="nav-menu-desktop">
            <h3>Acesso rápido</h3>           
                <a href="/contato">
                    <li>Contato</li>
                </a>

                <a href="/minha-casa-minha-vida">
                    <li>Casa Verde e Amarela</li>
                </a>
                <a href="/sobre-a-c-a-c">
                    <li>Sobre C.A.C</li>
                </a>
                <a href="/imoveis">
                    <li>Imóveis</li>
                </a>
                <a href="/politica-de-privacidade">
                    <li>Política de Privacidade C.A.C</li>
                </a>
        </div>


    </nav>
</menu>