<footer id="footer" class="hide-desktop">
    <section>
        <div class="_form_infos">
            <div class="_form_footer bg_">
                <?php

                $logo_principal = get_field('logo_footer_mobile', 'option');
                if (have_rows('secao_footer', 'options')) :
                    while (have_rows('secao_footer', 'options')) : the_row();
                ?>
                        <h3><?php echo get_sub_field('footer_texto'); ?></h3>
                <?php
                    endwhile;
                endif;
                ?>
                <?= do_shortcode('[contact-form-7 id="183" title="cta footer"]'); ?>
            </div>
            <div class="_info_footer">
                
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>" alt="<?php bloginfo('name'); ?>" >
                        <img src="/wp-content/themes/construcao_open/assets/logocac.png" class="footer__logo"/>
                    </a>
               
                <div class="_info_controller">
                    <div class="pages_presentation">
                        <h3 class="acesso_rapido">Acesso Rápido</h3>
                        <ul class="footer__menu">
                            <?php
                            if (have_posts()) :
                                while (have_posts()) : the_post();
                                endwhile;
                            ?>

                                <?php $args = [
                                    'posts_per_page' => -1,
                                    'title_li' => '',
                                ];
                                wp_list_pages($args); ?>

                            <?php
                            else :
                                echo '<p>There are no pages!</p>';
                            endif;
                            ?>
                        </ul>

                    </div>
                    <div class="contact">
                        <h3>
                            Entre em contato e agende sua visita.
                        </h3>
                        <div class="footer__infos">


                            <h4>Sede Administrativa</h4>
                            <p>Minas Gerais: <a href="tel:+55313063-0081">(31) 3063-0081</a>
                                <br>
                                Central de Vendas: <a href="tel:+550800 944 0081">0800 944 0081</a>
                            </p>

                            <p>Rua Gabriela de Melo, 351
                                <br>
                                CEP 30.390-080 Bairro Olhos D'água - Belo Horizonte MG
                            </p>




                        </div>

                    </div>
                </div>
                <?php
                //redes sociais
                get_template_part('template_part/components/social_media');
                ?>

                    <h6 class="footer__copyright"> COPYRIGHT CAC ENGENHARIA LTDA 2014 – TODOS OS DIREITOS RESERVADOS.
                        </h6>
                
            </div>
        </div>





    </section>
</footer>