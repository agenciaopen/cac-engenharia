<?php
$logo_principal = get_field('logo_principal', 'option');
$favicon = get_field('favicon', 'option');
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PZFQ3CW');</script>
    <!-- End Google Tag Manager -->
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '314652509209691');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=314652509209691&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php bloginfo('name'); ?> | <?php wp_title('&#124;', true, 'right'); ?></title>
    <?php wp_head();?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/build/css/'.add_file_min().'.min.css'; ?>" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/build/css/custom.min.css'; ?>" type="text/css" media="all">
    
    <link rel="icon" sizes="16x16 32x32" type="image/x-icon" href="<?php if ($favicon != '') { echo $favicon; } else { echo get_template_directory_uri().'/build/imgs/demo/favicon.png'; } ?>"> 
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    
    <link href="https://cacengenhariabot.azurewebsites.net/CSS/botchat.css" rel="stylesheet" />
    <div id="bot"></div>
    <?php
        if(is_single()){
            echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/build/css/single.min.css" type="text/css" media="all">';
        }else{
            //echo "no link";
        }
    ?>
    <script src="//cacengenharia.housecrm.com.br/track/origem-1.6.js"></script>
    <script language="javascript"> 
        var hc_dominio_chat="//cacengenharia.housecrm.com.br"; 
        var hc_https = 1;
    </script> 
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/wp-content/themes/construcao_open/assets/ajustes.css" type="text/css" media="all">   
</head>
<body <?php body_class(); ?>>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PZFQ3CW"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

<header>
    <?php get_template_part('template_part/layout/nav'); ?>
    <?php get_template_part('template_part/mobile_components/nav'); ?>
</header>

<?php
if($logo_principal != ''){
?>
    <?php if(is_home()){echo '<section class="swiper-container banner">';} else {echo '<section class="banner-category">';} ?>
        <?php if(is_home()){get_template_part('template_part/components/main_banner');} else {get_template_part('template_part/components/category_banner');} ?>
    </section>
<?php
    } else{ ?>
    <section class="no-banner">
    </section>
<?php
    }
?>

<?php if (is_category()): ?>
<section class="main-title relative cf">
	<h1 class="title relative"><?php echo titulo_dinamico(); ?></h1>
	<?php if (is_page()) { ?><p class="subtitle df df-vc df-hb"><?php echo get_field('adicionar_subtitulo'); ?></p><?php } ?>
</section>

<section class="breadcrumb-section relative cf">
	<?php wp_custom_breadcrumbs(); ?>
</section>

<?php endif; ?>