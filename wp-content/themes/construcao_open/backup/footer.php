            <?php 
                $logo_principal = get_field('logo_footer', 'option');

                if( have_rows('secao_footer', 'options') ):
                while( have_rows('secao_footer', 'options') ): the_row(); 
            ?>
            <style>
                ._form_footer.bg_{
                    background-image:url(<?php echo get_sub_field('footer_background'); ?>);
                }
            </style>
            <?php 
                endwhile; 
                endif; 
            ?>

<footer id="footer" class="hide-mobile">
    <section>
        <div class="_side_logo_social">
            <ul>
                <li>
                    <div class="logo">
                        <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name');?>" alt="<?php bloginfo('name');?>">
                            <img src="<?php if ($logo_principal != '') {echo $logo_principal;} else {echo get_template_directory_uri() . '/build/imgs/demo/logo.png';}?>" alt="<?php bloginfo('name');?>" />
                        </a>
                    </div>
                </li>
            </ul>
            <?php
                //redes sociais
                get_template_part('template_part/components/social_media');
            ?>
        </div>
        <div class="_form_infos">
            <div class="_form_footer bg_">
                <?php 
                    if( have_rows('secao_footer', 'options') ):
                    while( have_rows('secao_footer', 'options') ): the_row(); 
                ?>
                <h3><?php echo get_sub_field('footer_texto'); ?></h3>
            <?php 
                endwhile; 
                endif; 
            ?>
                <?= do_shortcode('[contact-form-7 id="183" title="cta footer"]'); ?>
            </div>
            <div class="_info_footer">
                <div class="_info_controller">
                    <div class="pages_presentation">
                            <h3>Acesso Rápido</h3>
                            <ul>
                                <?php 
                                    if ( have_posts() ) :
                                        while ( have_posts() ) : the_post();
                                    endwhile;
                                ?>
        
                                <?php $args = [
                                            'posts_per_page' => -1,
                                            'title_li' => '',
                                        ];
                                        wp_list_pages( $args ); ?>
                            
                                <?php
                                    else :
                                        echo '<p>There are no pages!</p>';
                                    endif;
                                ?>
                            </ul>      
                            <div class="copy">
                                    <p> COPYRIGHT CAC ENGENHARIA LTDA 2014 – TODOS OS DIREITOS RESERVADOS.
                                    </p>
                            </div>
                    </div>
                    <div class="contact">
                        <h3>
                            Entre em contato e agende sua visita.
                        </h3>
                        <div class="two_in">
                                    <div>
                                        <ul>
                                            <li><h4>Sede Administrativa</h4></li>
                                            <li><p>MG (31) 3063-0081</p></li>
                                        </ul>
                                    </div>
                                    <div>
                                        <ul>
                                            <li><h4>Central de Vendas</h4></li>
                                            <li><p>0800 944 0081</p></li>
                                        </ul>
                                    </div>

                                    <div class="copy">
                                    <p>RUA GABRIELA DE MELO, 351, CEP 30.390-080
BAIRRO OLHOS D’ÁGUA – BELO HORIZONTE / MG
                                    </p>
                            </div>
                        </div>
                        <div class="copy_logo">
                            <img src="<?php echo get_site_url(); ?>/wp-content/themes/construcao/config/src/logo-open-01.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>

<?php

get_template_part('template_part/mobile_components/footer');
wp_footer(); 

?>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/build/js-lib/'.add_file_min().'-lib.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri().'/build/js/'.add_file_min().'.min.js'; ?>"></script>

<?php if(is_page_template( )){ ?>  <?php } else if( is_single()) { ?> <?php } else { ?>  <script type="text/javascript" src="<?php echo get_template_directory_uri().'/build/js/scripts.min.js'; ?>"></script> <?php } ?> 


<script>
    if(document.getElementById('info_tab')){
        var footer = document.getElementById('footer');
        footer.classList.add('footer-tabbed');
   }


$(window).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
            //$('#header').addClass('sticky');
            $('.mobile-nav-wrap').addClass('sticky');
        } else {
            //$('#header').removeClass('sticky');
            $('.mobile-nav-wrap').removeClass('sticky');
        }
    });

$(window).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
            $('#header').addClass('sticky');
            $('.mobile-nav-wrap').addClass('sticky');
        } else {
            $('#header').removeClass('sticky');
            $('.mobile-nav-wrap').removeClass('sticky');
        }
    });


</script>

</body>
</html>
