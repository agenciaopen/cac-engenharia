(function () {
    if(document.getElementById('info_tab')){
        var footer = document.getElementById('footer');
        footer.classList.add('footer-tabbed');
   }

   $(window).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
            $('#header').addClass('sticky');
        } else {
            $('#header').removeClass('sticky');
        }
    });

    var pix_controller = window.devicePixelRatio;
    if(pix_controller == 1){
     
    } else if (pix_controller == 1.25) {
        var head_caller = document.getElementsByTagName('head')[0];
        head_caller.innerHTML += '<link rel="stylesheet" href="/wp-content/themes/construcao/build/css/media_x2.min.css" type="text/css" media="all">';
    }


    if ($('form input[type=file]').length > 0) {
        $('form input[type=file]').each(function (index, value) {
            var counter = $('input-file'+ index);
            $(this).addClass('hide');
            $(this).attr('id', 'searchfilter');
            $(this).attr('id', counter.selector);
            $(this).parent().prepend('<div class="file-class"><label class="input-controller" for="searchfilter"><span>Escolher arquivo</span></label> <label class="input-file inside"><span id="file-name">Nenhum arquivo selecionado</span><label></div>');
            var _this    = this,
                $label = $(this).prev();
            $label.attr('for', counter.selector);
            _this.addEventListener('change', function(){
                $label = $(this).prev().find( "#file-name" );
                $label.text(this.files[0].name);
            });
        });
    }
})();   


function callCustomSelects(n){
    var x, i, j, selElmnt, a, b, c, name = n;
    x = document.getElementsByClassName(name);
    for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
            if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML += ' <span id="reset-button" onclick="reset()"> Limpar FiltrO </span>';
                y = this.parentNode.getElementsByClassName("same-as-selected");
                for (k = 0; k < y.length; k++) {
                y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");
                break;
            }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function(e) {
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
        arrNo.push(i);
        } else {
        y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide");
        }
    }
    }
    document.addEventListener("click", closeAllSelect);
}


var reset = function(){
    window.location.replace('https://www.cacengenharia.com.br/');
};

callCustomSelects("sf-field-taxonomy-fase_da_obra");
callCustomSelects("sf-field-taxonomy-estado");
callCustomSelects("sf-field-taxonomy-cidade");
callCustomSelects("sf-field-taxonomy-quarto");



$('.second-level').prepend('<li class="back">Back</li>');

var toggleLevel = function(e){
	var l = e.data.level;
	var $menu = $('menu');
	if(l == 1){
		if($(e.target).hasClass('js-menu') || ($('.is-open').length && !$(e.target).parents('nav').length))
		$menu.toggleClass('is-open');
	}
	else if(l == 2){
		if($menu.hasClass('is-first-level')){
			$('.second-level, menu').removeClass('is-second-level is-first-level');
		}
		else{
			$menu.addClass('is-first-level').find($(this)).next('.second-level').addClass('is-second-level');
		}
	}
};

$('.first-level li > a, .back').on("click", { level: 2 }, toggleLevel);

$(document).on('click', { level: 1 }, toggleLevel);


// open mobile menu
$('.js-toggle-menu').click(function(e){
    e.preventDefault();
    $('.mobile-header-nav').slideToggle();
    $(this).toggleClass('open');
});



var form_listener = function(form_id){
    //console.log(form_id+" form input[type=submit]");

    $(form_id+" form input[type=submit]").click(function() {
        console.log(form_id+" submited");
        chama(form_id);
    });
};

var chama = function(form_listen) {

    var empreend = $('#codempreendimento');

    if($('#codempreendimento')){
        empreend.val();
    } else {
        empreend = "";
    }

    var e = $(form_listen+" form input")[5];
    var t = $(form_listen+" form input")[6];
    var c = $(form_listen+" form input")[7];
    var s = e.value;
    var l = t.value;
    var cn = c.value;
    var tcn = cn.replace('(', '').replace(')', '').replace('-', '').replace(' ', '').replace(' ', '').trim();
    var i = $("#hide_val").val();
    var n = hc_envia_mensagem("", s, l, "", tcn, "Cadastro Imóvel Open", "", "", "", "");
    
        if(empreend != ''){
           n = hc_envia_mensagem(empreend, s, l, "", tcn, "Cadastro Imóvel Open", "", "", "", "");
        } else {
           n = hc_envia_mensagem("", s, l, "", tcn, "Cadastro Open", 2812, "", "", "");
        }

        console.log(s, l, tcn);

        document.addEventListener( 'wpcf7submit', function( event ) {
            if(n != "ERRO"){
                window.location.replace('/obrigado/');
            }
        }, false );

};


//form_listener("#wpcf7-f121-o2");
form_listener("#wpcf7-f183-o3");
form_listener("#wpcf7-f281-o2");
form_listener("#wpcf7-f120-o2");
form_listener("#wpcf7-f358-o3");
form_listener("#wpcf7-f359-o4");
form_listener("#wpcf7-f360-o5");