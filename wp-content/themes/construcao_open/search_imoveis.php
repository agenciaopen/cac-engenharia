<?php get_header(); ?>

<h3 class="title-search"><?php echo get_field('filtro_texto_titulo', 'options'); ?></h3>

<section class="search">
    <p>Encontre seu imóvel</p>
    <div class="formulario">
        <?= do_shortcode('[searchandfilter slug="busca-imoveis-topo"]'); ?>
    </div>
</section>

<?php if(is_home()){ get_template_part('/template_part/components/tab_info'); }else{} ?>

<section id="resultados" class="results-fix">
    <div class="content-loop">
        <?php 
            if ( have_posts() ) : while ( have_posts() ) : the_post();
                $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
                $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
                $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
                $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
                $faixa_de_preco = wp_get_post_terms($post->ID, 'faixa_de_preco', array("fields" => "names"));
                $theid = get_the_ID();
                if($fase_da_obra[0] == "Lançamento"){$fase_da_obra[0] = "Lancamento";}else if($fase_da_obra[0] == "Breve Lançamento") { $fase_da_obra[0] = "Breve_Lancamento";} else if($fase_da_obra[0] == "Documentação Grátis") { $fase_da_obra[0] = "Documentacao_gratis";} else {};
                if($quarto[0] == "01"){ $quarto[0] = "01 Quarto";}else if($quarto[0] == "02"){ $quarto[0] = "02 Quartos";} else if ($quarto[0] == "03"){ $quarto[0] = "03 Quartos";} else if ($quarto[0] == "04"){ $quarto[0] = "04 Quartos";} else if ($quarto[0] == "05"){ $quarto[0] = "05 Quartos";}else if ($quarto[0] == "06"){ $quarto[0] = "06 Quartos";};

            ?>

        <style>
        .image_results_controller._<?php echo $theid;

        ?> {
            background-image: url(<?= the_post_thumbnail_url();
            ?>);
        }
        </style>

        <div id="loop-emp" class="results-list">
            <div class="search-filter-result-item post">
                <a href="<?php the_permalink(); ?>">
                    <div class="image_results_controller _<?php echo $theid; ?>"></div>
                    <?php if($cidade[0] && $estado[0]) {?>
                    <div class="result_content_controller">
                        <?php if(get_field('residencial')){ ?><p><?php echo get_field('residencial') ?></p>
                        <?php }else{} ?>
                        <?php if($cidade[0] && $estado[0]) { ?><h3><?= $cidade[0]; ?><span><?= $estado[0]; ?></span>
                        </h3> <?php } else {} ?>
                        <?php if(get_field('condominio')) { ?><p> Condomínio <?php echo get_field('condominio') ?></p>
                        <?php } else {} ?>
                        <?php
                                        if($quarto[0]){
                                        while( have_rows('icones_blocos', 'options') ): the_row(); 
                                    ?>

                        <?php if($fase_da_obra[0] or get_field('itbi_info')){?>
                        <div class="tags">
                            <?php if($fase_da_obra[0]) { ?><p class="tag on <?= $fase_da_obra[0] ?>"><?php if($fase_da_obra[0] == "Lancamento"){echo $fase_da_obra[0] = "Lançamento";} else if($fase_da_obra[0] == "Breve_Lancamento"){echo $fase_da_obra[0] = "Breve Lançamento";}  else if($fase_da_obra[0] == "Documentacao_gratis") { $fase_da_obra[0] = "Documentação Grátis";} else { echo $fase_da_obra[0]; }; ?></p> <?php } else {} ?>
                            <?php if(get_field('itbi_info')) { ?><p
                                class="tag off <?php if( get_field('itbi_info') == "Documentação Grátis" ) { echo "Documentacao_gratis"; } else {  echo get_field('itbi_info');  } ?>">
                                <?php echo get_field('itbi_info'); ?></p> <?php } else {} ?>
                        </div>
                        <?php }else{} ?>

                        <style>
                        .icon.icon-moby {
                            background-image: url(<?php echo get_sub_field('quartos_icon')['url'];
                            ?>);
                        }

                        .icon.icon-req {
                            background-image: url(<?php echo get_sub_field('medidas_icon')['url'];
                            ?>);
                        }
                        </style>
                        <?php
                                        endwhile;
                                    }else{}
                                    ?>

                        <?php if($quarto[0]){?><p><i class="icon icon-moby"></i><?= $quarto[0]; ?> </p>
                        <?php } else {} ?>
                        <?php if(get_field('medidas') && get_field('medidas_2')){ ?>
                        <div class="medidas">
                            <p class="m2"><i class="icon icon-req"></i><?php echo get_field('medidas') ?> m</p>
                            <?php if(get_field('medidas_2')){?><p class="m2 left">a <?php echo get_field('medidas_2') ?>
                                m</p><?php }else{}?>
                        </div>
                        <?php } else {}?>
                    </div>
                    <?php } else { echo "preencha, cidade e estado."; } ?>
                </a>
            </div>
        </div>
        <?php endwhile; 
    endif;  ?>
    </div>
</section>
<?php get_footer(); ?>

<script>
    if($('#resultados')){
        $('html, body').animate({
            scrollTop: $("#resultados").offset().top
        }, 1000);     
    } 
</script>