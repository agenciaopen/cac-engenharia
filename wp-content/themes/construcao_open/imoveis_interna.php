<?php get_template_part('template_part/variables/variables'); ?>

<section class="has-container">
    <style>
    #crumbs:before {
        background-image: url(<?php echo get_field('icone_breadcrumb', 'option');
        ?>);
        content: '';
        display: block;
        width: 1.25em;
        height: 1.25em;
        background-size: 100%;
        background-repeat: no-repeat;
        float: left;
        margin: 0 1% 0 0;
    } 
    </style>
    <?php wp_custom_breadcrumbs(); ?>

    <?php 
        $fase_da_obra = wp_get_post_terms($post->ID, 'fase_da_obra', array("fields" => "names"));
        $estado = wp_get_post_terms($post->ID, 'estado', array("fields" => "names"));
        $cidade = wp_get_post_terms($post->ID, 'cidade', array("fields" => "names"));
        $quarto = wp_get_post_terms($post->ID, 'quarto', array("fields" => "names"));
    
    if($fase_da_obra[0] == $estado){ ?>
    <?php } else {?>

    <?php if($fase_da_obra[0] or get_field('itbi_info') or get_field('condominio')){?>
    <h1 class="title_imv"><?php echo get_field('titulo_pagina'); ?></h1>
    <div class="tags">
        <?php if($fase_da_obra[0]) { ?><p class="tag interna <?= $fase_da_obra[0] ?>">
            <?php if($fase_da_obra[0] == "Lancamento"){echo $fase_da_obra[0] = "Lançamento";}else { echo $fase_da_obra[0]; }; ?>
        </p> <?php } else {} ?>
        <?php if(get_field('condominio')) { ?><p class="tag interna <?php echo get_field('condominio'); ?>">Condomínio
            <?php echo get_field('condominio'); ?></p> <?php } else {} ?>
        <?php if(get_field('tag_interna')) { ?><p class="tag interna <?php echo get_field('tag_interna'); ?>">
            <?php echo get_field('tag_interna'); ?></p> <?php } else {} ?>
        <?php while( have_rows('tags_imovel') ): the_row(); ?>
        <?php if(get_sub_field('tag_1') or get_sub_field('tag_2') or get_sub_field('tag_3') or get_sub_field('tag_4') or get_sub_field('tag_5')) { ?>
        <?php if(get_sub_field('tag_1')) {?><p class="tag interna <?php echo get_sub_field('tag_1'); ?>">
            <?php echo get_sub_field('tag_1'); ?></p><?php } else{} ?>
        <?php if(get_sub_field('tag_2')) {?><p class="tag interna <?php echo get_sub_field('tag_2'); ?>">
            <?php echo get_sub_field('tag_2'); ?></p><?php } else{} ?>
        <?php if(get_sub_field('tag_3')) {?><p class="tag interna <?php echo get_sub_field('tag_3'); ?>">
            <?php echo get_sub_field('tag_3'); ?></p><?php } else{} ?>
        <?php if(get_sub_field('tag_4')) {?><p class="tag interna <?php echo get_sub_field('tag_4'); ?>">
            <?php echo get_sub_field('tag_4'); ?></p><?php } else{} ?>
        <?php if(get_sub_field('tag_5')) {?><p class="tag interna <?php echo get_sub_field('tag_5'); ?>">
            <?php echo get_sub_field('tag_5'); ?></p><?php } else{} ?>
        <?php } else {} ?>
        <?php endwhile; ?>
        <?php if(get_field('itbi_info')) { ?><p class="tag interna <?php echo get_field('itbi_info'); ?>">
            <?php echo get_field('itbi_info'); ?></p> <?php } else {} ?>
    </div>
    <?php }else{} ?>

    <?php } ?>
</section>

<?php  
    get_template_part('template_part/components/tab_info');
    //components page
    get_template_part('template_part/components/page_intern_imv/section_about_imv'); 
    if($fase_da_obra[0] == "Breve Lançamento"){ 
        get_template_part('template_part/components/page_intern_imv/progress_bar');
    } else {
        get_template_part('template_part/components/page_intern_imv/section_differentials'); 
        get_template_part('template_part/components/page_intern_imv/image_gallery');
        get_template_part('template_part/components/page_intern_imv/progress_bar');
        get_template_part('template_part/components/page_intern_imv/map_in_imv');
    }
    get_template_part('template_part/components/page_intern_imv/section_cta_imv');
    //get_template_part('template_part/components/page_intern_imv/view_fixed_imv_suggest');
?>

<input type="hidden" id="codempreendimento" value="<?php echo get_field('codempreendimento'); ?>">

<section class="full-container-no-bg">
    <h4>Veja outros imóveis semelhantes a esse!</h4>
    <div class="swiper-container launch">
        <?php get_template_part('/template_part/components/show_launch'); ?>
        <div class="swiper-button-prev launch"></div>
        <div class="swiper-button-next launch"></div>
    </div>
</section>



<?php if(is_single()){ ?>
<script src="<?php echo get_template_directory_uri().'/build/js/imask.min.js'; ?>"></script>
<script src="<?php echo get_template_directory_uri().'/build/js/single.js'; ?>"></script>
<?php } else {} ?>


<script>

var toggleLevel = function(e){
	var l = e.data.level;
	var $menu = $('menu');
	if(l == 1){
		if($(e.target).hasClass('js-menu') || ($('.is-open').length && !$(e.target).parents('nav').length))
		$menu.toggleClass('is-open');
	}
	else if(l == 2){
		if($menu.hasClass('is-first-level')){
			$('.second-level, menu').removeClass('is-second-level is-first-level');
		}
		else{
			$menu.addClass('is-first-level').find($(this)).next('.second-level').addClass('is-second-level');
		}
	}
}

$('.first-level li > a, .back').on("click", { level: 2 }, toggleLevel);

$(document).on('click', { level: 1 }, toggleLevel);


// open mobile menu
$('.js-toggle-menu').click(function(e){
    e.preventDefault();
    $('.mobile-header-nav').slideToggle();
    $(this).toggleClass('open');
});


var form_listener = function(form_id){
    //console.log(form_id+" form input[type=submit]");

    $(form_id+" form input[type=submit]").click(function() {
        console.log(form_id+" submited");
        chama(form_id);
    });
};

var chama = function(form_listen) {

    var empreend = $('#codempreendimento').val();

    if($('#wpcf7-f120-o2 form textarea')[0] != undefined){
        var msg = $('#wpcf7-f120-o2 form textarea')[0].value;
    } else if($('#wpcf7-f121-o2 form input')[9] != undefined && $('#wpcf7-f121-o2 form input')[10] != undefined) {
        var msg_one = $('#wpcf7-f121-o2 form input')[9].value;
        var msg_two = $('#wpcf7-f121-o2 form input')[10].value;
        msg = msg_one, msg_two;
    } else {
        msg = "Cadastro Imóvel Open";
    }

    var e = $(form_listen+" form input")[5];
    var t = $(form_listen+" form input")[6];
    var c = $(form_listen+" form input")[7];
    var s = e.value;
    var l = t.value;
    var cn = c.value;
    var tcn = cn.replace('(', '').replace(')', '').replace('-', '').replace(' ', '').replace(' ', '').trim();
    var i = $("#hide_val").val();
    var n = hc_envia_mensagem(<?php echo get_field('codempreendimento'); ?>, s, l, "", tcn, msg, <?php if($estado[0] == 'MG') {?> 2812 <?php } ?> <?php if($estado[0] == 'RJ') {?> 3055 <?php } ?>, "", "", "");
    
        if(empreend != undefined && empreend != ''){
           n = hc_envia_mensagem(<?php echo get_field('codempreendimento'); ?>, s, l, "", tcn, msg, <?php if($estado[0] == 'MG') {?> 2812 <?php } ?> <?php if($estado[0] == 'RJ') {?> 3055 <?php } ?>, "", "", "");
           console.log(<?php echo get_field('codempreendimento'); ?>, s, l, tcn, msg);
        } else {
           n = hc_envia_mensagem("", s, l, "", tcn, msg, <?php if($estado[0] == 'MG') {?> 2812 <?php } ?> <?php if($estado[0] == 'RJ') {?> 3055 <?php } ?>, "", "", "");
           console.log(<?php echo get_field('codempreendimento'); ?>, s, l, tcn, msg, "sem empreemdimento");
        }

        document.addEventListener( 'wpcf7submit', function( event ) {
            if(n != "ERRO"){
                window.location.replace('/obrigado/');
            }
        }, true );

};

form_listener("#wpcf7-f281-o2");

</script>