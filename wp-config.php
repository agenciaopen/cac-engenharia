<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'open_cac' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');


@ini_set( 'upload_max_filesize' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'memory_limit', '256M' );
@ini_set( 'max_execution_time', '300' );
@ini_set( 'max_input_time', '300' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-mYk8fOcXe1w~2jpNe)z8ac=h!EffAlS0Lm??4wtmk,Yh>8dcK(nQusLP@pZEIlq' );
define( 'SECURE_AUTH_KEY',  'w A:d1%Ohq,4}k.0<hHNA945 C/=Gi/Q.I }Z6sybjp2_nrL-xjOYC/zpTNZ*vk!' );
define( 'LOGGED_IN_KEY',    '(WjQgro[PR9<e+jT+f#43e#=APH.2M-{-I<P+yn%T0hTM0D[l5HZ5IU)OnUmd3]B' );
define( 'NONCE_KEY',        'x|>E.^DM-rs^,^Qu/#,Vp>k1cB3`kFfv(Eme(|wgf:uX~%8[ih=oQv9KM214.7-(' );
define( 'AUTH_SALT',        'dTPS =^]0{-4@;$09GhSjtB~F+exot~,yPWn?8Qpg= 3.Ww{I~!Q!HR+Ta)$#7Z{' );
define( 'SECURE_AUTH_SALT', 'a;3p&y]H$7:pY@9zHP|541I8HvN}6?A!f>8RZ+G:&1kk*bgiERv19mIUCe?PEpkN' );
define( 'LOGGED_IN_SALT',   '1&p*PVX;6]tAkj?`m=T7.!Z+#E5U~$Zvn%CdTGPeh%teEkfHTipw2~x:L30J,&6P' );
define( 'NONCE_SALT',       'I$#=k0k=wTASH]w[2/:}&ohU#=6LL1$VsDGR~xv`uOhOLywbG?Qe3kyivP;vfw?^' );
define( 'WPMS_ON', false );
define( 'WPMS_SMTP_PASS', 'Opencacengenharia2019' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');


